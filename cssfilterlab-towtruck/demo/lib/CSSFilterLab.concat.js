/*! CSSFilterLab - v0.1.0 - 2013-06-06
* http://html.adobe.com/webplatform/graphics/customfilters/* Copyright (c) 2013 Adobe Systems Inc.; Licensed Apache License 2.0 */

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function () {
    
function builtin(fn, params) {
    return {
        fn: fn,
        params: params
    };
}

function color(isCss) {
    return {
        type: 'color',
        mixer: 'mixVector',
        generator: isCss ? 'color' : 'vector'
    };
}

function transform() {
    return {
        type: 'transform',
        generator: 'transform',
        mixer: {
            fn: 'mixHash',
            params: ['mixNumber']
        }
    };
}

function range(min, max, step) {
    return {
        type: 'range',
        min: min,
        max: max,
        step: step
    };
}

function vec(type, min, max, step) {
    return {
        type: type,
        generator: 'vector',
        mixer: 'mixVector',
        min: min,
        max: max,
        step: step
    };
}

function vec4(min, max, step) {
    return vec("vec4", min, max, step);
}

function vec3(min, max, step) {
    return vec("vec3", min, max, step);
}

function vec2(min, max, step) {
    return vec("vec2", min, max, step);
}

function units(unit, value) {
    value.unit = unit;
    return value;
}

function checkbox() {
    return {
        type: 'checkbox',
        mixer: 'dontMix'
    };
}

function mix(blendMode, compositeOperator) {
    return {
        blendMode: blendMode || "normal",
        compositeOperator: compositeOperator || "source-atop"
    };
}

function mesh(columns, rows) {
    return {
        columns: columns,
        rows: rows
    };
}

function builtinPercent(fn, defaultValue, maxValue) {
    return {
        type: builtin(fn, ["amount"]),
        params: {
            amount: defaultValue || 0
        },
        config: {
            amount: units("%", range(0, maxValue || 100, 1))
        }
    };
}

function builtinDeg(fn, defaultValue) {
    return {
        type: builtin(fn, ["angle"]),
        params: {
            angle: defaultValue || 0
        },
        config: {
            angle: units("deg", range(0, 360, 1))
        }
    };
}

function normalAmountConfig (defaultValue, min, max, step) {
    if (min === undefined) {
        min = 0.0;
    }
    
    if (max === undefined) {
        max = 0.0;
    }
    
    if (defaultValue === undefined) {
        defaultValue = (max - min) / 2;
    }
    
    if (step === undefined) {
        step = (max - min) / 100;
    }
    
    return {
        params: {
            amount: defaultValue
        },
        config: {
            amount: range(min, max, step)
        }
    };
}

window.filterConfigs = {
    'drop-shadow': {
        type: builtin("drop-shadow", ["offset_x", "offset_y", "radius", "flood_color"]),
        params: {
            offset_x: 5.0,
            offset_y: 5.0,
            radius: 5.0,
            flood_color: [0.0, 0.0, 0.0, 0.5]
        },
        config: {
            offset_x: units("px", range(-100, 100, 0.01)),
            offset_y: units("px", range(-100, 100, 0.01)),
            radius: units("px", range(0, 20, 0.01)),
            flood_color: color(true)
        }
    },
    
    "blur": {
        type: builtin("blur", ["deviation"]),
        params: {
            deviation: 3.0
        },
        config: {
            deviation: units("px", range(0, 10, 0.5))
        }
    },
    
    grayscale: builtinPercent("grayscale", 100),
    'hue-rotate': builtinDeg("hue-rotate", 180),
    invert: builtinPercent("invert", 100),
    opacity: builtinPercent("opacity", 50),
    saturate: builtinPercent("saturate", 1000, 1000),
    sepia: builtinPercent("sepia", 100),
    brightness: builtinPercent("brightness", 25),
    contrast:  builtinPercent("contrast", 50),
    
    warp: {
        hasVertex: true,
        hasFragment: true,
        mix: mix("normal"),
        mesh: mesh(20, 20),
        meshBox: "border-box",
        params: {
            k: null,
            matrix: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            useColoredBack: 1,
            backColor: [1.0, 1.0, 1.0, 1.0]
        },
        config: {
            k: {
                type: 'warp',
                generator: 'warpArray',
                mixer: 'mixVectorOfVectors'
            },
            matrix: transform(),
            useColoredBack: checkbox(),
            backColor: color()
        }
    },
    "rolling-scroll": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("normal"),
        mesh: mesh(1, 500),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0,
                perspective: 1000
            },
            initialRollSize: 0.05,
            rollRatio: 0.7,
            rollSeparation: 0.005,
            rollDepth: 500,
            useColoredBack: 1,
            backColor: [1.0, 1.0, 1.0, 0.9]
        },
        config: {
            transform: transform(),
            initialRollSize: range(0, 0.1, 0.0001),
            rollRatio: range(0, 1, 0.01),
            rollSeparation: range(0, 0.1, 0.0001),
            rollDepth: range(0, 2000, 1),
            useColoredBack: checkbox(),
            backColor: color()
        }
    },
    "fold": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(8, 50),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: -30,
                rotationZ: 0
            },
            t: 0.5,
            spins: 1.5,
            phase: -0.7,
            shadow: 1.5,
            mapDepth: 40,
            mapCurve: -0.3,
            minSpacing: 0.3,
            useColoredBack: 1,
            backColor: [0.5, 0.5, 0.5, 1.0]
        },
        config: {
            transform: transform(),
            t: range(0, 1, 0.01),
            spins: range(0.5, 10, 0.01),
            phase: range(-3.14, 3.14, 0.01),
            shadow: range(0, 2, 0.01),
            mapDepth: range(0.0, 200, 1),
            mapCurve: range(-0.5, 0.5, 0.01),
            minSpacing: range(0, 0.5, 0.01),
            useColoredBack: checkbox(),
            backColor: color()
        }
    },
    "tile-shuffle": {
        hasVertex: true,
        hasFragment: true,
        mix: mix(),
        mesh: mesh(100, 100),
        meshBox: "border-box detached",
        params: {
            matrix: {
                rotationX: 20,
                rotationY: 20,
                rotationZ: 20
            },
            amount: 250,
            t: 0.5
        },
        config: {
            matrix: transform(),
            amount: range(0, 500, 10),
            t: range(0, 1, 0.01)
        }
    },
    "tile-explosion": {
        hasVertex: true,
        hasFragment: true,
        mix: mix(),
        mesh: mesh(100, 100),
        meshBox: "border-box detached",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            tileTransform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            explosiveness: 1.5,
            tileRotation: [0.0, 0.0, 0.0],
            center: [0.5, 0.5],
            t: 0.2,
            fade: 0.8
        },
        config: {
            transform: transform(),
            tileTransform: transform(),
            explosiveness: range(0, 5, 0.1),
            tileRotation: vec3(-180, 180, 1),
            center: vec2(-1, 1, 0.01),
            t: range(0, 1, 0.01),
            fade: range(0, 1, 0.01)
        }
    },
    "crumple": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(50, 50),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            amount: 1, 
            strength: 0.4, 
            lightIntensity: 1.0
        },
        config: {
            transform: transform(),
            amount: range(0, 1, 0.01),
            strength: range(0.0, 10, 0.01),
            lightIntensity: range(0, 10, 0.01)
        }
    },
    "spherify": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(50, 50),
        meshBox: "border-box",
        params: {
            amount: 1,
            sphereRadius: 0.35,
            sphereAxis: [-0.25, 1.0, 0.0],
            sphereRotation: 90,
            ambientLight: 0.0,
            lightPosition: [1.0, -0.25, 0.25],
            lightColor: [1.0, 1.0, 1.0, 1.0],
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            }
        },
        config: {
            amount: range(0, 1, 0.01),
            sphereRadius: range(0, 0.5, 0.01),
            sphereAxis: vec3(-1, 1, 0.01),
            sphereRotation: range(0, 360, 1),
            ambientLight: range(0, 1, 0.01),
            lightPosition: vec3(-1, 1, 0.01),
            lightColor: color(),
            transform: transform()
        }
    },
    "tile-flip": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(32, 25),
        meshBox: "border-box detached",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            amount: 0.2,
            randomness: 2.0,
            flipAxis: [0.0, 1.0, 0.0],
            tileOutline: 1.0
        },
        config: {
            transform: transform(),
            amount: range(0, 1, 0.01),
            randomness: range(0, 3, 0.01),
            flipAxis: vec3(-1, 1, 0.01),
            tileOutline: checkbox()
        }
    },
    "burn": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(50, 1),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            amount: 1.0,
            randomSeed: 0.0
        },
        config: {
            transform: transform(),
            amount: range(0, 1, 0.01),
            randomSeed: range(0, 1, 0.01)
        }
    },
    "dissolve": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("multiply"),
        mesh: mesh(1, 1),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            amount: 0.3
        },
        config: {
            transform: transform(),
            amount: range(0, 1, 0.01)
        }
    },
    "page-curl": {
        hasVertex: true,
        hasFragment: true,
        mix: mix("normal"),
        mesh: mesh(50, 50),
        meshBox: "border-box",
        params: {
            transform: {
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            },
            curlPosition: [0.0, 0.0],
            curlDirection: 135,
            curlRadius: 0.2,
            bleedThrough: 0.5
        },
        config: {
            transform: transform(),
            curlPosition: vec2(-1, 1, 0.01),
            curlDirection: range(0, 360, 1.0),
            curlRadius: range(0.05, 3, 0.01),
            bleedThrough: range(0, 1, 0.01)
        }       
    },
    "curtains": {
        "hasVertex": true,
        "hasFragment": true,
        "mix": mix("multiply"),
        "mesh": mesh(100, 1),
        "meshBox": "border-box",
        "params": {
            "transform": {
                "rotationX": 0,
                "rotationY": 0,
                "rotationZ": 0,
                "perspective": 2000,
                "scale": 0.99
            },
            "numFolds": 5,
            "foldSize": 2.5,
            "amount": 0.3
        },
        "config": {
            "transform": transform(),
            "numFolds": range(1, 10, 1),
            "foldSize": range(1, 20, 0.1),
            "amount": range(0, 1, 0.01)
        }
    }
};
})();

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    var Global = window.Global = {};

    function objectCreateShim(proto) {
        if (Object.create)
            return Object.create(proto);
        function EmptyConstructor() {
        }
        EmptyConstructor.prototype = proto;
        return new EmptyConstructor();
    }

    if (!Function.prototype.bind) {
        Function.prototype.bind = function(thisObj) {
            var fn = this,
                argsToBind = Array.prototype.slice.call(arguments, 1);
            return function() {
                var fnArgs = Array.prototype.concat.call(argsToBind,
                    Array.prototype.slice.call(arguments, 0));
                fn.apply(thisObj, fnArgs);
            };
        };
    }

    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(obj) {
            for (var i = 0; i < this.length; ++i)
                if (this[i] == obj)
                    return i;
            return -1;
        };
    }

    if (!window.console) {
        window.console = {
            error: function() { },
            log: function() { }
        };
    }
    
    /*
     * Helper function to extend the prototype of a class from another base class
     * Global.Utils.extend(Cat).from(Animal);
     */
    Global.Utils = {

        extend: function(newClass) {
            return {
                from: function(baseClass) {
                    newClass.prototype = objectCreateShim(baseClass.prototype);
                    newClass.$super = baseClass;
                    newClass.prototype.$super = baseClass.prototype;
                }
            };
        },

        identity: function(a) { return a; },

        clone: function(a) {
            return $.extend(true, {}, a);
        },

        upperCaseFirstLetter: function(str) {
            if (!str.length)
                return str;
            return str.charAt(0).toUpperCase() + str.substr(1);
        },

        checkDefaultNumber: function(value, defaultValue) {
            value = parseFloat(value);
            return isNaN(value) ? defaultValue : value;
        },

        generateBase64Alphabet: function() {
            var a = {},
                charCodeUpperA = "A".charCodeAt(0),
                charCodeLowerA = "a".charCodeAt(0) - 26,
                charCode0 = "0".charCodeAt(0) - 52,
                i;
            for (i = 0; i < 26; ++i)
                a[i] = String.fromCharCode(charCodeUpperA + i);
            for (i = 26; i < 52; ++i)
                a[i] = String.fromCharCode(charCodeLowerA + i);
            for (i = 52; i < 62; ++i)
                a[i] = String.fromCharCode(charCode0 + i);
            a[62] = "+";
            a[63] = "/";
            return a;
        },

        encodeBase64: function(val) {
            if (!this._base64Alphabet)
                this._base64Alphabet = this.generateBase64Alphabet();
            var result = "",
                alphabet = this._base64Alphabet;
            for (var i = 0; i < val.length; i += 3) {
                // 1111 11 | 11 2222 | 22 22 33 | 33 3333
                // 1111 11 | 22 2222 | 33 33 33 | 44 4444
                var remaining = val.length - i,
                    a = val.charCodeAt(i),
                    b = (remaining > 1) ? val.charCodeAt(i + 1) : 0,
                    c = (remaining > 2) ? val.charCodeAt(i + 2) : 0,
                    x1 = (a & 0xFC) >> 2,
                    x2 = ((a & 0x3) << 4) | ((b & 0xF0) >> 4),
                    x3 = ((b & 0xF) << 2) | ((c & 0xC0) >> 6),
                    x4 = c & 0x3F;

                switch (remaining) {
                    case 1:
                        result += alphabet[x1] + alphabet[x2] + "==";
                        break;
                    case 2:
                        result += alphabet[x1] + alphabet[x2] + alphabet[x3] + "=";
                        break;
                    default:
                        result += alphabet[x1] + alphabet[x2] + alphabet[x3] + alphabet[x4];
                }
            }
            return result;
        }

    };

    if (!Function.prototype.bind) {
        Function.prototype.bind = function(thisObj) {
            var args = Array.prototype.slice.call(arguments, 1),
                fn = this;
            return function() {
                return fn.apply(thisObj, args.concat(Array.prototype.slice.call(arguments, 0)));
            }
        }
    }

})();

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
	
	function EventDispatcher() {
		this._events = {};
	}

	EventDispatcher.prototype = {
		fire: function(name, data) {
			var listeners = this.getListenersList(name),
				self = this;
			if (!listeners)
				return;
			listeners = $.map(listeners, function(fn) { return fn; });
			$.each(listeners, function(i, fn) {
				if (data)
					fn.apply(this, data);
				else
					fn.call(this);
			});
		},

		getListenersList: function(name, create) {
			if (create && !this._events[name])
				this._events[name] = [];
			return this._events[name];
		},

		on: function(name, fn) {
			var listeners = this.getListenersList(name, true);
			if (listeners.indexOf(fn) != -1)
				return;
			listeners.push(fn);
			return fn;
		},

		off: function(name, fn) {
			var listeners = this.getListenersList(name),
				index = listeners.indexOf(fn);
			if (index == -1)
				return;
			listeners.splice(index, 1);
			return fn;
		}
	};

	Global.EventDispatcher = EventDispatcher;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    function Application() {
        this.checkFeatures();

        $("#loading").remove();
        this.mainViewEl = $("#main-view").show();

        this.browserPopupCloseEl = $("#browser-popup-close");
        this.browserPopupCloseEl.click(this.onBrowserPopupCloseClicked.bind(this));
        this.browserPopupEl = $("#browser-popup").detach();
        
        this.config = new Global.Config();
        this.config.load(filterConfigs);
        this.github = new Global.GitHub();

        this.fileSystem = new Global.LocalStorage();
        this.filterStore = new Global.FilterStore(this.config, this.fileSystem, this.github);
        this.filterList = new Global.FilterList(this.filterStore);
        this.animation = new Global.Animation(this.filterList, this.filterStore);
        this.presets = new Global.PresetStore(this.fileSystem, this.filterStore, this.animation);

        this.timelineView = new Global.TimelineView(this.animation);
        this.shaderEditorView = new Global.ShaderEditorView(this.filterStore, this.github);
        this.activeFilterListView = new Global.ActiveFilterListView(this.filterList);
        
        this.builtinFilterStoreView = new Global.FilterStoreView(this.filterStore, this.filterList, this.activeFilterListView, this.shaderEditorView, "builtins");
        this.customFilterStoreView = new Global.FilterStoreView(this.filterStore, this.filterList, this.activeFilterListView, this.shaderEditorView, "custom");
        this.forkedFilterStoreView = new Global.FilterStoreView(this.filterStore, this.filterList, this.activeFilterListView, this.shaderEditorView, "forked");
        this.importFilterView = new Global.ImportFilterView(this.filterStore);

        this.cssCodeView = new Global.CSSCodeView(this.animation);
        this.presetStoreView = new Global.PresetStoreView(this.presets);

        this.dockView = new Global.DockView($("#app"));
        this.filterDock = new Global.DockView($("#filter-dock").removeClass("hidden"));

        this.logoView = new Global.LogoView(this.filterDock, this.filterStore);
        this.logo = $("a.logo");
        this.helpView = new Global.HelpView(this);

        this.githubPanel = new Global.DockPanel("GitHub");
        this.githubPanel.el.append($("#fork-github").detach());

        this.containerDockPanel = new Global.DockPanel("Document");
        this.targetEl = $("#container");
        $("#main").appendTo(this.containerDockPanel.el);

        this.unsupportedPopupEl = $("#filters-not-supported-popup").detach();

        this.companyLogo = $("#company-logo").detach();
        if (this.companyLogo.size())
            $(document.body).addClass("hosted-version");

        // At this point we are finished with loading HTML snippets, so we can remove the components element.
        $("#components").remove();

        this.init();
    }
    
    Application.prototype = {
        init: function() {
            this.dockView
                .addVerticalColumn()
                    .setMinSize(350)
                    .setWidth(350)
                    .addContainer()
                        .setFixed()
                        .setHeight(45)
                        .add(this.logoView.dockPanel)
                        .removeTabs()
                    .column
                    .addContainer()
                        .add(this.activeFilterListView.dockPanel)
                        .setIsDocumentArea()
                        .removeTabs()
                    .column
                    .addContainer(true)
                        .setFixed()
                        .setHeight(35)
                        .add(this.githubPanel)
                        .removeTabs();

            this.dockView
                .addVerticalColumn()
                    .setMinSize(400)
                    .setIsDocumentArea().addClass('light')
                    .addContainer()
                        .setFixed()
                        .setHeight(45)
                        .add(this.presetStoreView.dockPanel)
                        .removeTabs()
                        .column
                    .addContainer()
                        .add(this.containerDockPanel)
                        .setIsDocumentArea()
                        .removeTabs()
                    .column.addContainer()
                            .setHeight(150)
                            .add(this.cssCodeView.dockPanelCode)
                            .add(this.cssCodeView.dockPanelAnimationCode)
                    .column.addContainer(true)
                            .setFixed()
                            .setHeight(50)
                            .add(this.timelineView.dockPanel)
                            .removeTabs()
                    .column
                        .addElements(this.companyLogo);

            // Shader editor will later on add more panels when the first
            // filter is going to be customized.
            this.shaderEditorView.dockView = this.dockView; 
            
            this.filterDock.add(this.builtinFilterStoreView.dockPanel)
                            .add(this.customFilterStoreView.dockPanel)
                            .add(this.forkedFilterStoreView.dockPanel)
                            .add(this.importFilterView.dockPanel);

            // Force a 3d layer in order to run filters in GPU.
            this.targetEl.css("-webkit-transform", "translate3d(0,0,0)");
            this.animation.on("filtersUpdated", this.onFiltersUpdated.bind(this));
            
            // Make sure the filters are up to date after all the events are set up.
            this.animation.update();
            this.filterStore.loadFilterConfigurations();
            this.setupFilterSelectListeners();

            this.checkMinimumFeatures();
        },

        onFiltersUpdated: function(cssFilters, filterCodeHtml, animationCodeHtml) {
            this.targetEl.css("-webkit-filter", cssFilters);
            this.presetStoreView.enableSaveAs()  
        },
        
        //
        // Feature detection:
        //

        prefixes: ["", "-webkit-", "-moz-", "-ms-", "-o-"],
        
        checkFeatureWithPropertyPrefix: function(property, value) {
            var div = $("<div />");
            for (var i = 0; i < this.prefixes.length; ++i) {
                var prefixedProperty = this.prefixes[i] + property;
                if (div.css(prefixedProperty, value).css(prefixedProperty) == value)
                    return true;
            }
            return false;
        },

        checkFeatureWithValuePrefix: function(property, value) {
            var div = $("<div />");
            for (var i = 0; i < this.prefixes.length; ++i) {
                var prefixedValue = this.prefixes[i] + value;
                if (div.css(property, prefixedValue).css(property) == prefixedValue)
                    return true;
            }
            return false;
        },

        checkWebGLSupport: function(property, value) {
	var isCanvasSupported = true;
	var isWebGlSupported = true;
    var canvas, context;
	try {
		canvas = document.createElement('canvas');
	}
	catch(err) {
		isCanvasSupported = false;
	}	
	try {
		context = canvas.getContext("experimental-webgl");
	}
	catch(err) {
		isWebGlSupported = false;
	}
	if (!(isCanvasSupported && isWebGlSupported))
		return null;
	return !!context;
        },

        checkFeatures: function() {
            this.supportsFlex = this.checkFeatureWithValuePrefix("display", "flex") || this.checkFeatureWithValuePrefix("display", "box");
            this.supportsFilters = this.checkFeatureWithPropertyPrefix("filter", "sepia(100%)");
            this.supportsCustomFilters = this.checkFeatureWithPropertyPrefix("filter", "custom(none mix(url(http://www.example.com/)))");
            this.supportsWebGL = this.checkWebGLSupport();
        },

        checkMinimumFeatures: function() {
            if (!this.supportsFlex || !this.supportsFilters)
                this.showBrowserCheckPopup();
            else
                this.checkFirstTimeLoad();

            if (!this.supportsCustomFilters || !this.supportsWebGL) {
                var message;
                if (!this.supportsCustomFilters)
                    message = "Your browser does not support custom filters.";
                else if (!this.supportsWebGL) {
                    // Supports custom filters, but WebGL isn't enabled.
                    message = "You need to enable WebGL to use custom filters.";
                }

                this.unsupportedPopupEl.find(".message").html(message);
                this.unsupportedPopupEl.find(".filters-not-supported-help").click(this.showCustomFilterCheckPopup.bind(this));
                this.customFilterStoreView.filterStockListEl.replaceWith(this.unsupportedPopupEl.clone(true));
                
                // hide tabs for unavailable features
                this.importFilterView.dockPanel.tabEl.hide()
                this.forkedFilterStoreView.dockPanel.tabEl.hide()
            }
        },

        showBrowserCheckPopup: function() {
            this.mainViewEl.hide();
            this.browserPopupCloseEl.hide();
            this.browserPopupEl.appendTo(document.body).show();
        },

        showCustomFilterCheckPopup: function() {
            this.browserPopupCloseEl.show();
            this.browserPopupEl.appendTo(document.body).show();
        },

        onBrowserPopupCloseClicked: function() {
            this.browserPopupEl.detach();
            return false;
        },
        
        setupFilterSelectListeners: function(){
            var self = this;
            $.each([this.builtinFilterStoreView, this.customFilterStoreView, this.forkedFilterStoreView], function(index, view){
                view.on("filterSelected", function(){
                    self.logoView.hideFilterDock.call(self.logoView)
                });
            });
        },

        showFirstRunPopup: function() {
            this.firstRun = true;
            this.helpView.show();
        },

        checkFirstTimeLoad: function() {
            var self = this;
            this.firstRun = false;
            this.fileSystem.get("first_run", function(err, data) {
                if (data != "no")
                    self.showFirstRunPopup();
            });
        }
    };

    if (!window.QUnit) {
        // Start the application only when QUnit is not loaded.
        $(function() {
            var app = window.app = new Application();
        });
    }

    Global.Application = Application;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 (function() {

    function BaseControl(delegate, name, config) {
        this.delegate = delegate;
        this.name = name;
        this.config = config;
        this.params = null;
        this.field = config.hasOwnProperty("field") ? config.field : name;
    }

    BaseControl.prototype = {
        setSource: function(params) {
            this.params = params;
            this._updateControls();
        },

        _updateControls: function() { },

        setValue: function(value) {
            if (!this.params || this.params[this.field] == value)
                return;
            this.params[this.field] = value;
            this._updateControls();
            this.onValueChange();
        },

        onValueChange: function() {
            if (this.params && this.delegate && this.delegate.valuesUpdated)
                this.delegate.valuesUpdated(this.name);
        },

        getValue: function(value) {
            if (!this.params)
                return;
            return this.params[this.field];
        },

        pushControls: function(parent) { },

        pushTableColumns: function(parent, element, label) {
            if (!label) {
                $("<td colspan='2' class='field-column' />").append(element).appendTo(parent);
            } else {
                $("<td class='field-column' />").append(element).appendTo(parent);
                $("<td class='field-value-label' />").append(label).appendTo(parent);
            }
        },

        createEditableLabel: function(label) {
            return new Global.EditableLabel(this, label);
        },

        getValueForLabelEditor: function() {
            return this.getValue();
        },

        setValueFromLabelEditor: function(value) {
            this.setValue(value);
        }
    };

    var Controls = {
        _registeredControls: {},
        register: function(name, control) {
            this._registeredControls[name] = control;
        },

        get: function(typeName) {
            return this._registeredControls[typeName];
        }
    };

    Global.BaseControl = BaseControl;
    Global.Controls = Controls;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function CodeEditor(el) {
        CodeEditor.$super.call(this);

        // For performance reasons we will keep the code-mirror editor in a absolute posioned element
        // attached directly to body. This way we will avoid long layouts and long paint times due to
        // flex-box implementation.
        // We will keep the original element around to make it easy to steal its coordinates.
        this.uiElement = el;

        this.element = $("<div />").addClass("code-editor").appendTo("#codemirror-editors-view");
        this.isSupported = true;
             
        if (typeof CodeMirror !== 'function'){
            console.warn("Missing CodeMirror library. See README file")
            this.isSupported = false;
            return
        }   

        var self = this;
        this.editor = new CodeMirror(this.element[0], {
            lineNumbers: true,
            matchBrackets: true,
            mode: "text/x-csrc"
        });
        this.editor.on("change", function() {
            self.fire("valueChanged", [self.getValue()]);
        });
    }

    Global.Utils.extend(CodeEditor).from(Global.EventDispatcher);

    $.extend(CodeEditor.prototype, {
        setValue: function(value) {
            this.editor.setValue(value);
        },

        getValue: function() {
            return this.editor.getValue();
        },

        setActive: function(active) {
            this.element.toggleClass("active-shader-view", active);
            if (active)
                this.refresh();
        },

        refresh: function() {
            if (!this.isSupported){
                return
            }
            var clientRect = this.uiElement[0].getBoundingClientRect();
            this.element.css({
                "left": clientRect.left,
                "top": clientRect.top,
                "width": clientRect.width,
                "height": clientRect.height
            });
            $(this.editor.getScrollerElement()).height(clientRect.height);
            this.editor.refresh();
        }
    });

    Global.CodeEditor = CodeEditor;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function MultiControl(delegate, name, config) {
        MultiControl.$super.call(this, delegate, name, config);
    }

    Global.Utils.extend(MultiControl).from(Global.BaseControl);

    MultiControl.prototype.createControls = function(controlConfig) {
        this.controls = $("<table class='multi-control-table' />");

        var subControls = [],
            self = this;

        $.each(controlConfig, function(name, config) {
            var ControlClass = Global.Controls.get(config.type);
            if (!ControlClass)
                return;
            
            var control = new ControlClass(self, name, config);
            subControls.push(control);

            var controlRowEl = $("<tr />").appendTo(self.controls),
                label = $("<td />").text(name).appendTo(controlRowEl);
            
            control.pushControls(controlRowEl);
            if (config.after)
                controlRowEl.append(config.after);
        });

        this.subControls = subControls;
    }

    MultiControl.prototype.valuesUpdated = function() {
        this.onValueChange();
        this.onMultiControlChanged();
    }

    MultiControl.prototype._updateControls = function() {
        var value = this.getValue();
        if (!value)
            return;
        this.onMultiControlChanged();
        $.each(this.subControls, function(i, control) {
            control.setSource(value);
        });
    }

    MultiControl.prototype.onMultiControlChanged = function() { }

    MultiControl.prototype.pushControls = function(parent) {
        this.pushTableColumns(parent, this.controls);
    }

    Global.MultiControl = MultiControl;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function ColorControl(delegate, name, config) {
        ColorControl.$super.call(this, delegate, name, config);
        this.init();
    }

    Global.Utils.extend(ColorControl).from(Global.MultiControl);

    ColorControl.prototype.colorRange = function(field, after) {
        return {
            type: "range",
            min: 0,
            max: 1,
            step: 0.01,
            field: field,
            after: after
        };
    }

    ColorControl.prototype.init = function() {
        var self = this;

        this.colorColumnEl = $("<div class='color-preview' rowspan='4' />");
        this.colorPreviewEl = $("<input type='color' class='param-color-preview' />")
            .appendTo(this.colorColumnEl);
        
        this.hasColorInput = (function(self){
            var smiley = ":)"                   

            // a valid color input will sanitize the smiley
            return smiley !== self.colorPreviewEl.val(smiley).val()
        })(this);

        if (!this.hasColorInput)
            this.colorPreviewEl.attr("disabled", "disabled");

        this.colorPreviewEl.change(function () { self._onChange(); });

        this.createControls({
            "r": this.colorRange(0),
            "g": this.colorRange(1),
            "b": this.colorRange(2),
            "a": this.colorRange(3)
        });

        this.label = $("<span class='value-label'/>");
        this.editableLabel = this.createEditableLabel(this.label);
        this.controls.prepend(
            $("<tr/ >")
                .append($("<td />").text("Color"))
                .append($("<td />").append(this.colorColumnEl))
                .append($("<td class='field-value-label' />").append(this.label))
        );

        this.controls.addClass("param-color");
    }
    
    ColorControl.prototype._onChange = function() {
        var value = this.getValue();
        if (!value)
            return;
        var color = parseInt(this.colorPreviewEl.val().substr(1), 16);
        value[0] = ((color & 0xFF0000) >> 16) / 255;
        value[1] = ((color & 0x00FF00) >> 8) / 255;
        value[2] = ((color & 0x0000FF)) / 255;
        this.onValueChange();
        this._updateControls();
    }

    ColorControl.prototype.getValueForLabelEditor = function() {
        var v =  this.getValue();
        if (!v)
            return;

        var colors = [Math.floor(v[0] * 255),
                      Math.floor(v[1] * 255),
                      Math.floor(v[2] * 255)];
        var val = "#" +
                toBase16(colors[0]) +
                toBase16(colors[1]) +
                toBase16(colors[2]);
        
        return val;
    }

    ColorControl.prototype.setValueFromLabelEditor = function(inputValue) {
        var value = this.getValue();
        if (!value)
            return;
        if (inputValue.length < 7 || inputValue.charAt(0) != "#")
            return;
        var color = parseInt(inputValue.substr(1), 16);
        if (isNaN(color))
            return;

        value[0] = ((color & 0xFF0000) >> 16) / 255;
        value[1] = ((color & 0x00FF00) >> 8) / 255;
        value[2] = ((color & 0x0000FF)) / 255;
        
        this.onValueChange();
        this._updateControls();
    }

    function toBase16(val) {
        var b16 = Math.round(val).toString(16);
        return (b16.length == 1) ? "0" + b16 : b16;
    }

    ColorControl.prototype.onMultiControlChanged = function() {
        var v = this.getValue();
        if (!v)
            return;
        var colors = [Math.floor(v[0] * 255),
                      Math.floor(v[1] * 255),
                      Math.floor(v[2] * 255),
                      v[3]],
            colorCSS = "rgba(" + colors.join(", ") + ")";
        this.colorPreviewEl.css("background-color", "none");
        this.colorPreviewEl.css("background", "-webkit-linear-gradient(top, "+ colorCSS +" 0%, " + colorCSS + " 100%), url('style/img/color_bg.png')");

        var val = "#" +
                toBase16(colors[0]) +
                toBase16(colors[1]) +
                toBase16(colors[2]);
        this.colorPreviewEl.val(val);

        this.label.html(val);
     }

    Global.Controls.register("color", ColorControl);

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function CheckboxControl(delegate, name, config) {
        CheckboxControl.$super.call(this, delegate, name, config);
        this.init();
    }

    CheckboxControl.lastEditorId = 0;

    Global.Utils.extend(CheckboxControl).from(Global.BaseControl);

    CheckboxControl.prototype.init = function() {
        var self = this,
            name = "checkbox-" + this.name + (CheckboxControl.lastEditorId++);
        this.ctrl = $("<input type='checkbox' />")
                    .attr("id", name)
                    .attr("name", name);
        this.ctrlLabel = $("<label />").attr("for", name).html("&nbsp;");

        this.ctrlParent = $("<div >").append(this.ctrl).append(this.ctrlLabel);

        this.ctrl.bind("change blur", function () { self._onChange(); });
    }

    CheckboxControl.prototype._onChange = function() {
        var val = this.ctrl.attr("checked") == "checked";
        this.setValue(val ? 1 : 0, false);
    };

    CheckboxControl.prototype._updateControls = function() {
        var value = this.getValue();
        if (!value)
            this.ctrl.removeAttr("checked");
        else    
            this.ctrl.attr("checked", "checked");
    }

    CheckboxControl.prototype.pushControls = function(parent) {
        this.pushTableColumns(parent, this.ctrlParent);
    }

    Global.Controls.register("checkbox", CheckboxControl);


})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function vectorControlFactory(itemsCount) {

        function VectorControl(delegate, name, config) {
            VectorControl.$super.call(this, delegate, name, config);
            this.init();
        }

        Global.Utils.extend(VectorControl).from(Global.MultiControl);

        VectorControl.prototype.itemRange = function(field) {
            return {
                type: "range",
                min: this.config.min,
                max: this.config.max,
                step: this.config.step,
                field: field
            };
        }

        VectorControl.prototype.updateConfig = function(desc) {
            $.each(this.subControls, function(i, control) {
                control.updateConfig(desc);
            });
        }

        VectorControl.prototype.init = function() {
            var self = this,
                items = {},
                labels = ["x", "y", "z", "w"];
            for (var i = 0; i < itemsCount; ++i)
                items[labels[i]] = this.itemRange(i);
            this.createControls(items);
        }

        return VectorControl;
    }
    

    Global.Controls.register("vec2", vectorControlFactory(2));
    Global.Controls.register("vec3", vectorControlFactory(3));
    Global.Controls.register("vec4", vectorControlFactory(4));

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    /*
     * binding should provide a "setValue" and "getValue".
     */
    function EditableLabel(binding, label) {
        this.binding = binding;
        this.label = label;
        this.init();
    }

    EditableLabel.prototype = {
        init: function() {
            var self = this;
            this.label.click(function() {
                if (self.binding.canEdit && !self.binding.canEdit())
                    return false;
                self.label.hide();
                self.createEditorIfNeeded();
                self.editor.val(self.binding.getValueForLabelEditor());
                self.editor.show().select();
                return false;
            });
        },

        createEditorIfNeeded: function() {
            if (this.editor)
                return;
            var self = this;
            this.editor = $("<input type='text' class='value-editor' />")
                .hide()
                .change(function() {
                    return self.onChange();
                })
                .focusout(function() {
                    self.hide();
                }).keyup(function (e) {
                    self.onKeyUp(e);
                });
            this.label.after(this.editor);
        },

        hide: function() {
            this.editor.hide();
            this.label.show();
        },

        onKeyUp: function(e) {
            if (e.which == 13) {
                this.hide();
                e.preventDefault();
            }
            if (!this.onChange()) {
                // Rewrite last known good value.
                this.editor.val(this.binding.getValue());
                this.editor.select();
            }
        },

        onChange: function() {
            var newVal = this.editor.val();
            if (this.binding.validate && ((newVal = this.binding.validate(newVal)) === null))
                return false;
            this.binding.setValueFromLabelEditor(newVal);
            return true;
        }
    }

    Global.EditableLabel = EditableLabel;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function RangeControl(delegate, name, config) {
        RangeControl.$super.call(this, delegate, name, config);
        this.init();
    }

    Global.Utils.extend(RangeControl).from(Global.BaseControl);

    RangeControl.prototype.init = function() {
        var self = this;

        this.label = $("<span class='value-label'/>");
        this.editableLabel = this.createEditableLabel(this.label);

        this.ctrl = $("<input type='range' />")
                    .attr("id", "range-" + this.name)
                    .attr("class", "range-" + this.name)
                    .attr("value", "");
        this.updateConfig(this.config);
        this.ctrl.change(function () { self._onChange(); });
    }

    RangeControl.prototype.updateConfig = function(desc) {
        this.ctrl.attr("min", desc.min)
            .attr("max", desc.max)
            .attr("step", desc.step);
    }

    RangeControl.prototype._onChange = function() {
        var val = parseFloat(this.ctrl.val());
        if (isNaN(val))
            val = 0;
        this.setValue(val, false);
    }

    RangeControl.prototype._updateControls = function() {
        var value = parseFloat(this.getValue());
        if (isNaN(value))
            value = 0;
        this.label.html(Math.round(100 * value) / 100);
        this.ctrl.val(value);
    }

    RangeControl.prototype.pushControls = function(parent) {
        this.pushTableColumns(parent, this.ctrl, this.label);
    }

    RangeControl.prototype.validate = function(value) {
        var val = parseFloat(value);
        if (isNaN(value) || value < this.min || value > this.max)
            return null;
        return val;
    }
    
    Global.Controls.register("range", RangeControl);

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function TransformControl(delegate, name, config) {
        TransformControl.$super.call(this, delegate, name, config);
        this.init();
    }

    Global.Utils.extend(TransformControl).from(Global.MultiControl);

    TransformControl.prototype.range = function(field, min, max, step) {
        return {
            type: "range",
            min: min,
            max: max,
            step: step,
            field: field
        };
    }

    TransformControl.prototype.rotationRange = function(field) {
        return this.range(field, -180, 180, 1);
    }

    TransformControl.prototype.init = function() {
        this.createControls({
            "rotateX": this.rotationRange("rotationX"),
            "rotateY": this.rotationRange("rotationY"),
            "rotateZ": this.rotationRange("rotationZ"),
            "scale": this.range("scale", 0.0, 5, 0.1),
            "perspective": this.range("perspective", 0.0, 2000, 1)
        });
        this.controls.addClass("param-transform");
    }

    Global.Controls.register("transform", TransformControl);

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function TextControl(delegate, name, config) {
        TextControl.$super.call(this, delegate, name, config);
        this.init();
    }

    Global.Utils.extend(TextControl).from(Global.BaseControl);

    TextControl.prototype.init = function() {
        var self = this;

        this.validatorEl = $("<div />");

        this.ctrl = $("<input type='text' />")
                    .attr("id", "text-" + this.name)
                    .attr("value", "");
        this.ctrl.bind("change click blur", function (ev) { self._onChange(ev); });
    }

    TextControl.prototype._onChange = function(ev) {
        var val = this.ctrl.val();
        if (this.validate(val) === null) {
            if (ev.type == "blur")
                this._updateControls();
            return;
        }
        this.setValue(val, false);
    };

    TextControl.prototype._updateControls = function() {
        var value = this.getValue();
        this.ctrl.val(value);
    }

    TextControl.prototype.pushControls = function(parent) {
        this.pushTableColumns(parent, this.ctrl);
    }

    TextControl.prototype.validate = function(value) {
        this.validatorEl.css("-webkit-filter", "none");
        this.validatorEl.css("-webkit-filter", "custom(url(test), t " + value + ")");
        
        var parsedValue = this.validatorEl.css("-webkit-filter");
        console.log(parsedValue);
        if (parsedValue === null || parsedValue == "none")
            return null;
        return value;
    }
    
    Global.Controls.register("unknown", TextControl);
    Global.Controls.register("text", TextControl);

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function WarpControl(delegate, name, config) {
        WarpControl.$super.call(this, delegate, name, config);
        this.init();
    }

    Global.Utils.extend(WarpControl).from(Global.BaseControl);

    WarpControl.prototype.init = function() {
        var desc = this.config,
            self = this;

        this.ctrl = $("<div/>")
                    .attr("id", "param-" + this.name);

        this.canvas = Global.WarpHelpers.buildWarpCanvas(this.ctrl, 180, 180, function() {
            self._onChange();
        });
    }

    WarpControl.prototype._onChange = function() {
        this.onValueChange();
    }

    WarpControl.prototype._updateControls = function() {
        var value = this.getValue();
        if (!(value instanceof Array) && !(value instanceof Global.ActiveCollection))
            return;
        this.canvas.setPoints(value);
        this.canvas.redraw();
    }

    WarpControl.prototype.pushControls = function(parent) {
        this.pushTableColumns(parent, this.ctrl);
    }
    
    Global.Controls.register("warp", WarpControl);

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function ActiveObject(source, delegate) {
        ActiveObject.$super.call(this);
        this.delegate = delegate;
        this.properties = {};
        if (source)
            this.loadFromJSON(source);
    }

    function ActiveCollection(source, delegate) {
        ActiveCollection.$super.call(this, source, delegate);
    }

    Global.Utils.extend(ActiveObject).from(Global.EventDispatcher);
    Global.Utils.extend(ActiveCollection).from(ActiveObject);

    $.extend(ActiveObject.prototype, {
        loadFromJSON: function(source) {
            var self = this;
            $.each(source, function(key, val) {
                if (!self.properties.hasOwnProperty(key))
                    self.injectSetterMethod(key);
                self.properties[key] = self.wrapValue(val);
            });
        },

        injectSetterMethod: function(key) {
            Object.defineProperty(this, key, {
                get: this.get.bind(this, key),
                set: this.set.bind(this, key),
                enumerable: true,
                configurable: true
            });
        },

        wrapValue: function(val) {
            if (val !== null && typeof(val) == "object") {
                if (val instanceof Array)
                    val = new ActiveCollection(val, this);
                else if (!(val instanceof ActiveObject))
                    val = new ActiveObject(val, this);
            } 
            return val;
        },

        toJSON: function() {
            var result = {};
            $.each(this.properties, function(key, value) {
                if (value && value instanceof ActiveObject)
                    value = value.toJSON();
                result[key] = value;
            });
            return result;
        },

        fireRootValueChanged: function() {
            var root = this;
            while (root.delegate)
                root = root.delegate;
            root.fire("rootValueChanged");
        },

        set: function(key, newValue) {
            var oldValue = this.properties[key],
                hadValue = this.properties.hasOwnProperty(key);
            if (!hadValue)
                this.injectSetterMethod(key);
            this.properties[key] = newValue = this.wrapValue(newValue);
            this.fire("propertyChanged." + key, [newValue, oldValue]);
            this.fire("propertyChanged", [key, newValue, oldValue]);
            if (!hadValue)
                this.fire("propertyAdded", [key, newValue]);
            this.fireRootValueChanged();
            return newValue;
        },

        get: function(key) {
            return this.properties[key];
        },

        remove: function(key) {
            var oldValue = this.properties[key],
                hadValue = this.properties.hasOwnProperty(key);
            if (!hadValue)
                return;
            delete this.properties[key];
            this.fire("propertyChanged." + key, [null, oldValue]);
            this.fire("propertyChanged", [key, null, oldValue]);
            this.fire("propertyRemoved", [key, oldValue]);
            this.fireRootValueChanged();
            return oldValue;
        },

        bind: function(key, fn) {
            fn(this.get(key), null);
            return this.on("propertyChanged." + key, fn);
        },

        unbind: function(key, fn) {
            return this.off("propertyChanged." + key, fn);
        },

        oneWayBind: function(key, el, fn) {
            this.bind(key, function(newValue, oldValue) {
                var value = el[fn].call(el);
                if (oldValue != null && value == newValue)
                    return;
                el[fn].call(el, newValue);
            });
            return this;
        },

        twoWayBind: function(key, el, fn, events) {
            var self = this;
            this.oneWayBind(key, el, fn);
            el.bind(events, function() {
                var value = el[fn].call(el);
                self.set(key, value);
            });
            return this;
        },

        bindToTextInput: function(key, el) {
            return this.twoWayBind(key, el, "val", "change keyup blur");
        },

        bindToSelectInput: function(key, el) {
            return this.twoWayBind(key, el, "val", "change keyup click blur");
        }
    });

    $.extend(ActiveCollection.prototype, {
        loadFromJSON: function(source) {
            this.$super.loadFromJSON.call(this, source);
            this.set("length", source.length);
        },

        toJSON: function() {
            var result = [];
            $.each(this.properties, function(key, value) {
                if (value && value instanceof ActiveObject)
                    value = value.toJSON();
                result.push(value);
            });
            return result;
        }
    });

    Global.ActiveObject = ActiveObject;
    Global.ActiveCollection = ActiveCollection;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function Animation(filterList, filterStore) {
        Animation.$super.call(this);

        this.filterList = filterList;
        this.filterList.on("filterAdded", this.onFilterAdded.bind(this));
        this.filterList.on("filterRemoved", this.onFilterRemoved.bind(this));
        this.filterList.on("keyframeUpdated", this.onKeyframeUpdated.bind(this));
        this.filterList.on("filterStateChanged", this.onFilterStateChanged.bind(this));
        this.filterList.on("filtersOrderChanged", this.onFiltersOrderChanged.bind(this));
        this.filterList.on("filterConfigChanged", this.onFilterConfigChanged.bind(this));

        this.filterStore = filterStore;
        this.filterStore.on("filterUpdated", this.updateFilters.bind(this));

        this._keyframes = [];
        this._currentTime = 0;
        this.duration = 1000;
        
        this.computeDefaultKeyframe();
    }

    Global.Utils.extend(Animation).from(Global.EventDispatcher);

    $.extend(Animation.prototype, {
        keyframes: function() {
            return this._keyframes;
        },

        currentTime: function() {
            return this._currentTime;
        },
        
        maxTime: function() {
            return 100;
        },

        /* Computes the keyframes in the neighborhood of the specified time.
         * - If there is a keyframe at the specific time it will just return an
         * array with a single keyframe.
         * - If there are no keyframes at the specific time it will return an
         * array with 3 elements:
         *   - 0: first keyframe
         *   - 1: second keyframe
         *   - 2: delta time between the keyframes, useful for blending operations.
         */
        _currentKeyframeSet: function(time) {
            var keyframes = this.keyframes();
            if (!keyframes.length)
                return null;

            if (keyframes[0].time > time)
                return null;
            
            if (keyframes[keyframes.length - 1].time < time)
                return null;
                
            for (var i = 0; i < keyframes.length; ++i) {
                if (keyframes[i].time > time)
                    break;
            }
            
            var key1 = keyframes[i - 1];
            if (key1.time == time)
                return [key1];

            var key2 = keyframes[i];

            if (key1.time == key2.time)
                return [key1];
            
            var a = (time - key1.time) / (key2.time - key1.time);
            return [key1, key2, a];
        },

        _currentKeyframe: function (time) {
            var set = this.currentKeyframeSet(time);
            if (set && set.lenght == 1)
                return set[0];
            return this.calculateKeyframe(time);
        },

        currentKeyframe: function(time) {
            if (time === undefined || time == this.currentTime())
                return this._cachedCurrentKeyFrame;
            return this._currentKeyframe(time);
        },
        
        currentKeyframeSet: function(time) {
            if (time === undefined || time == this.currentTime())
                return this._cachedCurrentKeyframeSet;
            return this._currentKeyframeSet(time);
        },

        prevKeyframe: function() {
            var keyframes = this.keyframes();
            if (!keyframes || !keyframes.length)
                return null;
            
            var time = this.currentTime();
            if (time <= keyframes[0].time)
                return null;
            if (time > keyframes[keyframes.length - 1].time)
                return keyframes[keyframes.length - 1];
            
            var set = this.currentKeyframeSet(time);
            if (!set)
                return null;
            if (set.length != 1)
                return set[0];
            
            var index = keyframes.indexOf(set[0]);
            if (index <= 0)
                return null;

            return keyframes[index - 1];
        },

        nextKeyframe: function() {
            var keyframes = this.keyframes();
            if (!keyframes || !keyframes.length)
                return null;
            
            var time = this.currentTime();
            if (time >= keyframes[keyframes.length - 1].time)
                return null;
            if (time < keyframes[0].time)
                return keyframes[0];
            
            var set = this.currentKeyframeSet(time);
            if (!set)
                return null;
            if (set.length != 1)
                return set[1];
                        
            var index = keyframes.indexOf(set[0]);
            if (index < 0 || index > keyframes.length - 2)
                return;
            
            return keyframes[index + 1];
        },
        
        updateCachedValues: function() {
            var oldKeyframe = this._cachedCurrentKeyFrame;
            
            var time = this.currentTime();
            this._cachedCurrentKeyframeSet = this._currentKeyframeSet(time);
            this._cachedCurrentKeyFrame = this._currentKeyframe(time);
            
            this.filterList.setSource(this._cachedCurrentKeyFrame);
            if (oldKeyframe !== this._cachedCurrentKeyFrame)
                this.fire("newFrameSelected", [this._cachedCurrentKeyFrame, oldKeyframe]);
        },

        update: function() {
            this.updateCachedValues();
            this.updateFilters();
            this.fire("timeUpdated");
        },

        setCurrentTime: function(newTime) {
            this._currentTime = newTime;
            this.update();
        },

        setActiveKeyframe: function(keyframe) {
            if (!keyframe)
                return;
            this.setCurrentTime(keyframe.time);
        },

        sortKeyframes: function() {
            var keyframes = this.keyframes();
            keyframes.sort(function(a, b) { return a.time - b.time; });
        },

        createKeyframe: function(time, source) {
            var keyframe = new Global.Keyframe(this.filterList, time, source),
                keyframes = this.keyframes();
            keyframes.push(keyframe);
            this.sortKeyframes();
            this.fire("keyframeAdded", [keyframe]);
            this.update();
            this.saveAnimation();
        },

        getFiltersAtTime: function(time, colorScheme) {
            var keyframe = this.currentKeyframe(time);
            return this.getFiltersForKeyframe(keyframe, colorScheme);
        },

        getFiltersForKeyframe: function(keyframe, colorScheme) {
            var filters = this.filterList.activeFilters(),
                value = keyframe.value,
                result = [];
            $.each(filters, function(i, filter) {
                result.push(filter.config.generateCode(value[filter.name], colorScheme));
            });
            return result;
        },

        computeAnimationCode: function(colorScheme) {
            var animation = [];
            if (!this.filterList.activeFilters().length)
                return animation;
            var maxTime = Math.max(1, this.maxTime()),
                self = this;
            $.each(this.keyframes(), function(index, keyframe) {
                var percent = Math.floor(keyframe.time / maxTime * 100) + "%",
                    filters = self.getFiltersForKeyframe(keyframe, colorScheme);
                animation.push(colorScheme.keyframe(percent, filters));
            });
            return animation;
        },

        updateFilters: function() {
            var time = this.currentTime(),
                filterProperty = this.getFiltersAtTime(time),
                filterHtml = this.getFiltersAtTime(time, Global.ColorSchemes.colorTheme),
                animationHtml = this.computeAnimationCode(Global.ColorSchemes.colorTheme);
            if (!filterHtml.length)
                filterHtml.push(Global.ColorSchemes.colorTheme.keyword("none"));
            this.fire("filtersUpdated", [
                        filterProperty.join(" "), 
                        "<div class='css-property-name'>-webkit-filter:</div><div class='css-property-value'>" + filterHtml.join("<br />") + ";</div>", 
                        animationHtml.join("")]);
        },

        computeDefaultKeyframe: function() {
            var self = this;
            this.defaultKeyframe = new Global.Keyframe(this.filterList, 0, {});
            $.each(this.filterList.filters, function(i, filter) {
                self.addFilterDefaultValues(filter);
            });
        },

        calculateKeyframe: function(time) {
            var set = this.currentKeyframeSet();
            if (!set) {
                var keyframes = this.keyframes();
                if (!keyframes.length)
                    return this.defaultKeyframe.makeGeneratedClone(time);
                
                if (keyframes[0].time > time)
                    return keyframes[0].makeGeneratedClone(time);
                if (keyframes[keyframes.length - 1].time < time)
                    return keyframes[keyframes.length - 1].makeGeneratedClone(time);
                throw new Error("ASSERT_NOT_REACHED");
            }
            if (set.length == 1)
                return set[0];
            
            return set[0].blend(set[1], set[2], time);
        },

        onFilterStateChanged: function(filter) {
            this.update();
            this.saveAnimation();
        },

        addFilterDefaultValues: function(filter) {
            $.each(this.keyframes(), function(index, keyframe) {
                keyframe.addFilterDefaultValue(filter);
            });
            this.defaultKeyframe.addFilterDefaultValue(filter);
            this.saveAnimation();
        },

        removeFilterValue: function(filter) {
            $.each(this.keyframes(), function(index, keyframe) {
                keyframe.removeFilterValue(filter);
            });
            this.defaultKeyframe.removeFilterValue(filter);
            this.saveAnimation();
        },

        reset: function() {
            var self = this,
                keyframes = this.keyframes();
            $.each(keyframes, function(index, keyframe) {
                self.fire("keyframeRemoved", [keyframe]);
            });
            keyframes.splice(0, keyframes.length);
            this.update();
            this.saveAnimation();
        },

        onKeyframeUpdated: function(keyframe, filter, paramName) {
            if (keyframe.generated)
                this.createKeyframe(keyframe.time, keyframe.value);
            else
                this.updateFilters();
            this.saveAnimation();
        },

        ignoreSaves: function() {
            if (this.savePending)
                clearTimeout(this.savePending);
            this._ignoreSaves = (this._savesIgnore || 0) + 1;
        },

        restoreSaves: function() {
            this._ignoreSaves = (this._savesIgnore || 0) - 1;
        },

        saveAnimation: function() {
            if (!this.store || this._ignoreSaves > 0)
                return;
            var self = this;
            if (this.savePending)
                clearTimeout(this.savePending);
            this.savePending = setTimeout(function() {
                self.store.saveAnimation();
            }, 100);
        },

        removeKeyframe: function(keyframe) {
            var keyframes = this.keyframes();
            var i = keyframes.indexOf(keyframe);
            if (i < 0)
                return;
            this.fire("keyframeRemoved", [keyframe]);
            keyframes.splice(i, 1);
            this.update();
            this.saveAnimation();
        },

        toggleKeyframe: function() {
            var keyframe = this.currentKeyframe();
            if (!keyframe)
                return;
            if (keyframe.generated)
                this.createKeyframe(keyframe.time, keyframe.value);
            else
                this.removeKeyframe(keyframe);
        },

        dumpAnimation: function() {
            var data = {
                filters: [],
                keyframes: [],
                duration: this.duration
            };
            $.each(this.filterList.filters, function(index, filter) {
                data.filters.push({
                    name: filter.name,
                    type: filter.config.name,
                    active: filter.active
                });
            });
            $.each(this.keyframes(), function(index, keyframe) {
                data.keyframes.push({
                    time: keyframe.time,
                    value: Global.Utils.clone(keyframe.value)
                });
            });
            return data;
        },

        setDuration: function(duration) {
            if (duration == this.duration)
                return;
            this.duration = duration;
            this.saveAnimation();
            this.fire("durationUpdated", [duration]);
        },

        loadAnimation: function(animation) {
            this.ignoreSaves();
            this.reset();
            this.filterList.reload(animation.filters);
            this.computeDefaultKeyframe();
            var self = this;
            $.each(animation.keyframes, function(i, keyframe) {
                self.createKeyframe(keyframe.time, keyframe.value);
            });
            this.setDuration(animation.duration);
            this.restoreSaves();
            this.update();
        },

        loadEmptyAnimation: function(animation) {
            this.loadAnimation({
                filters: [],
                keyframes: [],
                duration: 1000
            });
        },

        onFilterAdded: function(filter) {
            this.addFilterDefaultValues(filter);
            this.update();
            this.saveAnimation();
        },

        onFilterRemoved: function(filter) {
            this.removeFilterValue(filter);
            this.update();
            this.saveAnimation();
        },

        onFiltersOrderChanged: function() {
            this.updateFilters();
            this.saveAnimation();
        },

        onFilterConfigChanged: function(filter, configChange) {
            this.defaultKeyframe.updateKeyframeAfterConfigChange(filter, configChange);
            $.each(this.keyframes(), function(index, keyframe) {
                keyframe.updateKeyframeAfterConfigChange(filter, configChange);
            });
            this.update();
            this.saveAnimation();
        }
    });

    Global.Animation = Animation;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function PresetStore(fileSystem, filterStore, animation) {
        PresetStore.$super.call(this);

        this.fileSystem = fileSystem;
        this.filterStore = filterStore;
        this.animation = animation;
        animation.store = this;
        
        this.currentPreset = null;
        this.loaded = false;
        this.init();
    }

    Global.Utils.extend(PresetStore).from(Global.EventDispatcher);

    $.extend(PresetStore.prototype, {
        defaultPresetName: "Default",
        presetsFolder: "animations",

        presetFileName: function(preset) {
            return this.presetsFolder + "/" + encodeURIComponent(preset);
        },

        decodePresetName: function(fileName) {
            return decodeURIComponent(fileName);
        },

        init: function() {
            var self = this;
            this.filterStore.on("filtersLoaded", this.onFiltersLoaded.bind(this));
            this.fileSystem.register(this.onFileSystemCreated.bind(this));
        },

        onFileSystemCreated: function(fileSystem) {
            var self = this;
            this.fileSystem = fileSystem;

            this.fileSystemLoaded = true;
            this.loadPresetsIfPossible();
            
            $(window).bind("hashchange", function() {
                var animation = self.getPresetFromHash();
                if (!animation || animation == self.currentPreset)
                    return;
                self.loadAnimation(animation);
            });
        },

        onFiltersLoaded: function() {
            this.filtersLoaded = true;
            this.loadPresetsIfPossible();
        },

        getPresetFromHash: function() {
            var animation = window.location.hash;
            if (animation.length <= 1)
                return null;
            return decodeURIComponent(animation.substr(1));
        },

        loadPresetsIfPossible: function() {
            if (!this.fileSystemLoaded || !this.filtersLoaded)
                return;
            var self = this;
            this.loadPresets(function() {
                self.loadPreset(self.getPresetFromHash() || self.defaultPresetName);
                self.loaded = true;
                self.fire("presetsLoaded");
            });
        },

        loadPresets: function(callback) {
            var self = this;
            this.fileSystem.list(this.presetsFolder, function(err, list) {
                if (err) {
                    console.error(err);
                    return;
                }
                $.each(list, function(i, entry) {
                    self.fire("presetAdded", [self.decodePresetName(entry.name)]);
                });
                callback();
            });
        },

        setActivePreset: function(preset) {
            if (this.currentPreset == preset)
                return;
            var oldPreset = this.currentPreset;
            this.currentPreset = preset;
            this.updateHash();
            this.fire("activePresetChanged", [this.currentPreset, oldPreset]);
        },

        updateHash: function() {
            var animation = this.currentPreset;
            if (animation == this.defaultPresetName) {
                if (window.location.hash && window.location.hash.length)
                    window.location.hash = "";
                return;
            }
            var lastHash = encodeURIComponent(animation);
            window.location.hash = lastHash;
        },

        loadPreset: function(animation) {
            this.setActivePreset(animation);
            if (!this.fileSystem)
                return;
            var self = this;
            this.fileSystem.get(this.presetFileName(animation), function(err, result) {
                if (err) {
                    // File doesn't exist yet.
                    if (animation == self.defaultPresetName)
                        self.saveAnimation();
                    else
                        self.animation.loadEmptyAnimation();
                    return;
                }
                if (animation != self.currentPreset) {
                    // If the animation changed since we loaded it last time,
                    // avoid overwritting with some old values.
                    return;
                }
                var parsedAnimation = null;
                try {
                    parsedAnimation = JSON.parse(result);
                } catch (e) {
                    console.error("Error while parsing stored animation", result, e);
                }
                if (!parsedAnimation)
                    return;
                self.animation.loadAnimation(parsedAnimation);
            });
        },

        saveAnimation: function() {
            if (!this.loaded)
                return;
            var self = this,
                data = this.animation.dumpAnimation(),
                animation = this.currentPreset;
            this.fileSystem.save(this.presetFileName(animation), JSON.stringify(data), function(err) {
                if (err)
                    console.error("Error saving file " + animation, err);
            });
        },

        savePresetAs: function() {
            var newName = prompt("New preset name:");
            if (!newName)
                return;
            this.setActivePreset(newName);
            this.saveAnimation();
            return newName;
        },

        deletePreset: function(preset) {
            if (preset == this.defaultPresetName) {
                this.animation.loadEmptyAnimation();
                this.saveAnimation();
                return;
            }
            var self = this;
            this.loadPreset(this.defaultPresetName);
            this.fileSystem.deleteFile(this.presetFileName(preset), function(err) {
                // Done.
                self.fire("presetRemoved", [preset]);
            });
        }
    });

    Global.PresetStore = PresetStore;

})();

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function FilterConfig(filterName, jsonConfig) {
        FilterConfig.$super.call(this);
        if (jsonConfig.loading) {
            this.isLoading = true;
            this.isBuiltin = false;
            this.isFork = true;
            this.name = filterName;
            return;
        }
        this.init(filterName, jsonConfig);
    }

    Global.Utils.extend(FilterConfig).from(Global.EventDispatcher);

    $.extend(FilterConfig.prototype, {
        init: function(filterName, jsonConfig) {
            this.name = filterName;
            this.original = jsonConfig;
            this.mix = jsonConfig.mix;
            this.mesh = jsonConfig.mesh;
            this.meshBox = jsonConfig.meshBox;
            this.hasVertex = jsonConfig.hasVertex;
            this.hasFragment = jsonConfig.hasFragment;
            this.type = jsonConfig.type;
            this.label = jsonConfig.label || filterName;
            this.isBuiltin = FilterConfig.isBuiltinType(jsonConfig);
            this.params = {}
            this.config = {};

            var self = this;
            $.each(jsonConfig.params, function (name, defaultValue) {
                var filterConfig = jsonConfig.config[name];
                if (!filterConfig)
                    return;
                var filterParam = Global.Utils.clone(filterConfig),
                    type = filterParam.type || 'range',
                    mixer = (type == 'hidden' || type == 'unknown') ?
                            Global.mixers.dontMix :
                            Global.mixers.mixNumber,
                    generator = filterParam.generator || "identity";
                if (filterParam.mixer) {
                    if (filterParam.mixer.fn)
                        mixer = Global.mixers[filterParam.mixer.fn].apply(self, filterParam.mixer.params);
                    else
                        mixer = Global.mixers[filterParam.mixer];
                }
                filterParam.mixer = mixer;
                filterParam.generator = Global.CSSGenerators[generator] || Global.CSSGenerators.identity;
                self.config[name] = filterParam;
                self.params[name] = self.fixDefaultValue(type, defaultValue);
            });
        },

        defaultValuesByType: {
            transform: {
                perspective: 1000,
                scale: 1,
                rotationX: 0,
                rotationY: 0,
                rotationZ: 0
            }
        },

        fixDefaultValue: function(type, value) {
            var defaultValue = this.defaultValuesByType[type];
            if (!defaultValue || typeof(defaultValue) != "object")
                return value;
            $.each(defaultValue, function(key, val) {
                if (value.hasOwnProperty(key))
                    return;
                value[key] = val;
            });
            return value;
        },

        reload: function(config) {
            var wasLoading = this.isLoading,
                oldConfig = this.config,
                oldDefaults = this.defaultValues();
            this.isLoading = false;
            this.init(this.name, config);
            var newDefaults = this.defaultValues();
            var diff = this.compareDefaultValues(oldDefaults, newDefaults);
            if (oldConfig)
                this.addChangedTypesValues(oldConfig, diff, newDefaults);
            this.fire("configChanged", [diff, wasLoading]);
        },

        compareDefaultValues: function(oldVals, newVals) {
            var deletedKeys = [], addedValues = {};
            $.each(oldVals, function(name, value) {
                if (newVals.hasOwnProperty(name))
                    return;
                deletedKeys.push(name);
            });
            $.each(newVals, function(name, value) {
                if (oldVals.hasOwnProperty(name))
                    return;
                addedValues[name] = value;
            });
            return {
                deletedKeys: deletedKeys,
                addedValues: addedValues,
                changedValues: {}
            };
        },

        addChangedTypesValues: function(oldConfig, diff, newDefaults) {
            var self = this;
            $.each(oldConfig, function(key, oldParamConfig) {
                var newParamConfig = self.config[key];
                if (!newParamConfig || newParamConfig.type == oldParamConfig.type)
                    return;
                diff.changedValues[key] = newDefaults[key];
            });
        },

        defaultValues: function() {
            var defaults = {},
                self = this;
            if (this.isLoading)
                return defaults;
            $.each(this.params, function (name, value) {
                var filterParam = self.config[name];
                if (filterParam.type == 'warp' && !value)
                    value = Global.WarpHelpers.generateWarpPoints();
                defaults[name] = value;
            });
            return defaults;
        },

        blendParams: function(A, B, position) {
            var R = {},
                self = this;
            if (this.isLoading)
                return R;
            $.each(this.params, function (name) {
                var filterParam = self.config[name];
                R[name] = filterParam.mixer(A[name], B[name], position);
            });
            return R;
        },

        generatePreviewCode: function() {
            return this.generateCode(this.defaultValues());
        },

        generateCode: function(values, colors) {
            if (this.isLoading)
                return "";
            if (colors === undefined)
                colors = Global.ColorSchemes.noColoring;
            if (this.isBuiltin)
                return this.generateCodeForBuiltinType(values, colors);
            return this.generateCodeForShader(values, colors);
        },

        getVertexUrl: function() {
            if (this.edited_vertex)
                return this.edited_vertex;
            if (this.hasVertex)
                return "shaders/vertex/" + this.name + ".vs";
            return null;
        },

        getFragmentUrl: function() {
            if (this.edited_fragment)
                return this.edited_fragment;
            if (this.hasFragment || !this.hasVertex)
                return "shaders/fragment/" + this.name + ".fs"
            return null;
        },

        /**
         * Helper routine to change the custom shader parameters.
         */
        generateCodeForShader: function(values, colors) {
            var shaderParams = [],
                shaders = [],
                self = this,
                vertexUrl = this.getVertexUrl(),
                fragmentUrl = this.getFragmentUrl();
            shaders.push(vertexUrl ? colors.fn("url", [colors.uri(vertexUrl)]) : colors.keyword("none"));
            if (fragmentUrl) {
                var fragmentShader = colors.fn("url", [colors.uri(fragmentUrl)]);
                if (this.mix)
                    shaders.push(colors.fn("mix", [fragmentShader + " " + colors.keyword(this.mix.blendMode) + " " + colors.keyword(this.mix.compositeOperator)]));
                else
                    shaders.push(fragmentShader);
            }
            shaderParams.push(shaders.join(" "));

            var mesh = [];
            if (this.mesh)
                mesh.push(this.mesh.columns + " " + this.mesh.rows);
            if (this.meshBox)
                mesh.push(colors.keyword(this.meshBox));
            if (mesh.length)
                shaderParams.push(mesh.join(" "));
            
            $.each(this.params, function (name) {
                var paramConfig = self.config[name],
                    value = values[name];
                if (value === undefined || value === null)
                    return;
                var cssValue = colors.value(paramConfig.generator(value, colors), paramConfig ? paramConfig.unit : null);
                shaderParams.push(colors.parameterName(name) + " " + cssValue);
            });

            return colors.fn("custom", shaderParams);
        },

        /**
         * Helper routine to change the builtin filter parameters.
         */
        generateCodeForBuiltinType: function(values, colors) {
            var filterParams = [],
                self = this;
            $.each(this.params, function (name) {
                var paramConfig = self.config[name],
                    value = values[name];
                if (!paramConfig)
                    return;
                filterParams.push(colors.value(paramConfig.generator(value, colors), 
                    paramConfig ? paramConfig.unit : null));
            });
            
            return colors.builtinFn(this.type.fn, filterParams);
        },

        getData: function() {
            if (!this.data)
                this.data = {};
            return this.data;
        }
    });
    
    FilterConfig.isBuiltinType = function(config) {
        var type = config.type ? config.type.fn : "custom";
        return type != "custom";
    }

    Global.FilterConfig = FilterConfig;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){

    function Filter(name, config) {
        Filter.$super.call(this);
        this.name = name;
        this.config = config;
        this.active = false;
    }

    Global.Utils.extend(Filter).from(Global.EventDispatcher);

    $.extend(Filter.prototype, {
        filterType: function() {
            return this.config.type ? this.config.type.fn : "custom";
        },

        isBuiltin: function() {
            return this.config.isBuiltin;
        },

        isFork: function() {
            return !!this.config.isFork;
        },

        toggleFilter: function() {
            this.setActive(!this.active);
        },

        setActive: function(value) {
            if (this.active == value)
                return;
            this.active = value;
            this.fire("filterStateChanged", [this.active]);
        },

        setSource: function(value) {
            this.source = value;
            this.fire("filterSourceChanged", [value]);
        },

        valuesUpdated: function(paramName) {
            this.fire("valuesUpdated", [paramName]);
        },

        removeFilter: function() {
            this.fire("filterRemoved");
        }
    });

    Global.Filter = Filter;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function FilterStore(config, fileSystem, github) {
        FilterStore.$super.call(this);
        this.config = config;
        this.github = github;
        this.availableFilters = [];
        this.availableFiltersByName = {};
        this.lastFilterId = 0;
        this.fileSystem = fileSystem;
    }

    Global.Utils.extend(FilterStore).from(Global.EventDispatcher);

    
    $.extend(FilterStore.prototype, {
        forksFolder: "forks",
        shadersFolders: {
            vertex: "vertex",
            "fragment": "fragment"
        },

        defaultVertexShader: "void main() { }",
        defaultFragmentShader: "void main() { }",

        loadFilterConfigurations: function() {
            var self = this;
            $.each(this.config.filters, function(filterName, filterConfig) {
                self.addFilter(new Global.FilterConfig(filterName, filterConfig), true);
            });
            this.fire("filtersLoaded");
            this.fileSystem.register(this.onFileSystemCreated.bind(this));
        },

        onFileSystemCreated: function(fileSystem) {
            var self = this;
            this.fileSystem = fileSystem;
            // Make sure we have the directories first.
            this.fileSystem.createDirectory(this.shadersFolders["vertex"], function(err, dirEntry) {
                self.fileSystem.createDirectory(self.shadersFolders["fragment"], function(err, dirEntry) {
                    self.readForks();
                });
            });
        },

        readForks: function() {
            var self = this;
            this.fileSystem.list(this.forksFolder, function(err, list) {
                if (err) {
                    console.error(err);
                    return;
                }
                $.each(list, function(i, entry) {
                    self.loadFork(entry);
                });
            });
        },

        loadFork: function(entry) {
            var self = this,
                wait = 1,
                newFilterConfig,
                parsedForkJSON = null;
            this.fileSystem.readFile(entry, function(err, result) {
                if (err)
                    return;
                try {
                    parsedForkJSON = JSON.parse(result);
                } catch (e) {
                    console.error("Error while parsing stored filter config", result, e);
                    return;
                }
                newFilterConfig = self.loadedOrPendingFilterByName(parsedForkJSON.name);
                ++wait;
                
                self.fileSystem.getEntry(self.shadersFolders["vertex"] + "/" + encodeURIComponent(parsedForkJSON.name), function(err, entry) {
                    if (err)
                        return;
                    self.fileSystem.readFile(entry, function(err, result) {
                        newFilterConfig.editedSource_vertex = result;
                        newFilterConfig.edited_vertex = entry.toURL(result);
                        done();
                    });
                });
                
                ++wait;
                self.fileSystem.getEntry(self.shadersFolders["fragment"] + "/" + encodeURIComponent(parsedForkJSON.name), function(err, entry) {
                    if (err)
                        return;
                    self.fileSystem.readFile(entry, function(err, result) {
                        newFilterConfig.editedSource_fragment = result;
                        newFilterConfig.edited_fragment = entry.toURL(result);
                        done();
                    });
                });

                done();
            });

            function done() {
                if (--wait)
                    return;
                newFilterConfig.isFork = true;
                newFilterConfig.data = parsedForkJSON.data || {};
                newFilterConfig.reload(parsedForkJSON.value);
            }
        },

        loadFromGitHub: function(url, callback) {
            var self = this;
            this.github.readGist(url, function(err, response) {
                if (err)
                    return callback(err);
                var config = response.files["config.json"],
                    vertex = response.files["shader.vs"],
                    fragment = response.files["shader.fs"];
                if (!config || !vertex || !fragment)
                    return callback("Missing files in the specified gist.");
                var newJSONFilterConfig;
                try {
                    newJSONFilterConfig = JSON.parse(config["content"]);
                } catch (e) {
                    return callback("Could not parse config file.");
                }

                var forkJSON = { value: newJSONFilterConfig, name: self.allocateNewId(response.id), data: {} };
                if (!newJSONFilterConfig.label)
                    newJSONFilterConfig.label = forkJSON.name;
                newJSONFilterConfig.hasVertex = true;
                newJSONFilterConfig.hasFragment = true;
                
                var newFilterConfig = new Global.FilterConfig(forkJSON.name, newJSONFilterConfig);
                newFilterConfig.isFork = true;

                var wait = 2;
                
                self.saveShader(newFilterConfig, "vertex", vertex["content"], function(err) {
                    if (err) {
                        console.error("Failed to save vertex", err);
                        return;
                    }
                    done();
                });

                self.saveShader(newFilterConfig, "fragment", fragment["content"], function(err) {
                    if (err) {
                        console.error("Failed to save fragment", err);
                        return;
                    }
                    done();
                });
                
                function done() {
                    if (--wait)
                        return;
                    self.addFilter(newFilterConfig, false);
                    self.saveFork(forkJSON);
                    callback(null, newFilterConfig);
                }
            });
        },

        saveFork: function(forkJSON) {
            var data = JSON.stringify(forkJSON);
            this.fileSystem.save(this.forksFolder + "/" + encodeURIComponent(forkJSON.name), data,
                function(err, result) {
                    if (err) {
                        console.log("could not save fork " + forkJSON.name);
                        return;
                    }
                    // Done.
                });
        },
        
        deleteFromFileSystem: function(forkJSON){  
            this.fileSystem.deleteFile(this.forksFolder + "/" + encodeURIComponent(forkJSON.name), 
                function(err, result) {
                    if (err) {
                        console.log("could not delete fork " + forkJSON.name);
                        return;
                    }
                });         
        },

        addFilter: function(filterConfig, fromStore) {
            var key = "_" + filterConfig.name,
                filter = this.availableFiltersByName[key];
            if (filter)
                return filter;
            this.availableFilters.push(filterConfig);
            this.availableFiltersByName[key] = filterConfig;
            this.fire("filterAdded", [filterConfig, fromStore]);
            return filterConfig;
        },
        
        deleteFilter: function(filterConfig){
            var key = "_" + filterConfig.name,
                filter = this.availableFiltersByName[key];

            delete this.availableFiltersByName[key];
            this.availableFilters.filter(function(filter, index){
                if (filter.name === filterConfig.name){
                    this.availableFilters.splice(index, 1)
                }
            }.bind(this))       
            
            this.deleteFromFileSystem(filterConfig)
            this.fire("filterDeleted", [filterConfig]);
            filterConfig.fire("filterDeleted");
        },

        addLoadingFilter: function(filterName) {
            return this.addFilter(new Global.FilterConfig(filterName, {
                name: filterName,
                loading: true
            }), true);
        },

        filterConfigByName: function(filterName) {
            var key = "_" + filterName;
            return this.availableFiltersByName[key];
        },

        loadedOrPendingFilterByName: function(filterName) {
            var filterConfig = this.filterConfigByName(filterName);
            if (filterConfig)
                return filterConfig;
            // If the filter was not loaded yet, just return a promise
            // that the filter will be loaded.
            return this.addLoadingFilter(filterName);
        },

        saveShader: function(filter, type, value, callback) {
            var fileName = this.shadersFolders[type] + "/" + encodeURIComponent(filter.name);
            this.fileSystem.save(fileName, value, function(err, entry) {
                if (err) {
                    callback(err);
                    return;
                }
                filter["editedSource_" + type] = value;
                filter["edited_" + type] = entry.toURL(value);
                callback(null);
            });
        },

        filterUpdate: function(filter, type, value) {
            var self = this;
            this.saveShader(filter, type, value, function() {
                self.fire("filterUpdated", [filter]);
            })
        },

        forkConfigUpdate: function(filter) {
            this.saveFork({
                name: filter.name,
                value: filter.original,
                data: filter.data || {}
            });
        },

        allocateNewName: function(filterConfig) {
            return this.allocateNewId(filterConfig.name);
        },

        allocateNewId: function(id) {
            var name = id;
            while (this.filterConfigByName(name))
                name = id + (++this.lastFilterId);
            return name;
        },

        loadShaderFile: function(url, callback) {
            $.get(url, function(shader) {
                callback(null, shader);
            });
        },

        forkFilter: function(filterConfig) {
            var newJSONFilterConfig = Global.Utils.clone(filterConfig.original);
            var forkJSON = { value: newJSONFilterConfig, name: this.allocateNewName(filterConfig), data: {} };
            // The label inside newJSONFilterConfig can be changed by the user, but forkJSON.name will stay the same.
            delete newJSONFilterConfig["name"];
            newJSONFilterConfig.label = forkJSON.name;
            newJSONFilterConfig.hasVertex = true;
            newJSONFilterConfig.hasFragment = true;
            
            var newFilterConfig = new Global.FilterConfig(forkJSON.name, newJSONFilterConfig);
            newFilterConfig.isFork = true;
            newFilterConfig.forkedFilter = filterConfig;

            var wait = 1,
                self = this;

            if (filterConfig.hasVertex) {
                ++wait;
                this.loadShaderFile(filterConfig.getVertexUrl(), function(err, result) {
                    if (err)
                        return;
                    self.saveShader(newFilterConfig, "vertex", result, function(err) {
                        if (err) {
                            console.error("Failed to save vertex", err);
                            return;
                        }
                        done();
                    });
                });
            } else {
                newFilterConfig.editedSource_vertex = this.defaultVertexShader;
            }

            if (!filterConfig.hasVertex || filterConfig.hasFragment) {
                ++wait;
                this.loadShaderFile(filterConfig.getFragmentUrl(), function(err, result) {
                    if (err)
                        return;
                    self.saveShader(newFilterConfig, "fragment", result, function(err) {
                        if (err) {
                            console.error("Failed to save fragment", err);
                            return;
                        }
                        done();
                    });
                });
            } else {
                newFilterConfig.editedSource_fragment = this.defaultFragmentShader;
            }

            function done() {
                if (--wait)
                    return;
                self.addFilter(newFilterConfig, false);
                self.saveFork(forkJSON);
            }

            done();
        }

    });

    Global.FilterStore = FilterStore;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){

    function FilterList(filterStore) {
        FilterList.$super.call(this);
        this.filterStore = filterStore;

        this.lastFilterId = 0;
        this.filters = [];
        this.filtersById = {};
        this.filterConfigCallbacksById = {};
    }

    Global.Utils.extend(FilterList).from(Global.EventDispatcher);

    $.extend(FilterList.prototype, {
        updateFiltersOrder: function() {
            this.filters.sort(function(a, b) { return a.order - b.order; });
            this.fire("filtersOrderChanged");
        },

        allocateNewName: function(filterConfig) {
            var name = filterConfig.name;
            while (this.filterByName(name))
                name = filterConfig.name + (++this.lastFilterId);
            return name;
        },

        filterByName: function(name) {
            return this.filtersById["_" + name];
        },

        addFilter: function(filterConfig, filterName) {
            if (!filterName)
                filterName = this.allocateNewName(filterConfig);
            var filter = new Global.Filter(filterName, filterConfig);
            filter.on("valuesUpdated", this.onFilterValuesUpdated.bind(this, filter));
            filter.on("filterRemoved", this.removeFilter.bind(this, filter));
            filter.on("filterStateChanged", this.onFilterStateChanged.bind(this, filter));
            this.filters.push(filter);
            
            this.filtersById["_" + filterName] = filter;
            var filterConfigCallbacks = {
                filterConfigChanged: this.onFilterConfigChanged.bind(this, filter),
                filterConfigRemoved: this.onFilterConfigRemoved.bind(this, filter)
            };
            this.filterConfigCallbacksById["_" + filterName] = filterConfigCallbacks;

            filter.config.on("configChanged", filterConfigCallbacks.filterConfigChanged);
            filter.config.on("filterDeleted", filterConfigCallbacks.filterConfigRemoved);
            this.fire("filterAdded", [filter]);
            return filter;
        },

        removeFilter: function(filter) {
            var index = this.filters.indexOf(filter);
            if (index < 0)
                return;
            this.filters.splice(index, 1);
            console.log("removing " + filter.name);
            delete this.filtersById["_" + filter.name];
            var filterConfigCallbacks = this.filterConfigCallbacksById["_" + filter.name];
            filter.config.off("configChanged", filterConfigCallbacks.filterConfigChanged);
            filter.config.off("filterDeleted", filterConfigCallbacks.filterConfigRemoved);
            delete this.filterConfigCallbacksById["_" + filter.name];
            this.fire("filterRemoved", [filter, /*animation=*/true]);
        },

        reload: function(filters) {
            var self = this,
                oldFilters = this.filters;
            this.lastFilterId = 0;
            this.filters = [];
            this.filtersById = {};
            $.each(oldFilters, function(i, filter) {
                self.fire("filterRemoved", [filter, /*animation=*/false]);
            });
            $.each(filters, function(i, filter) {
                var filterConfig = self.filterStore.loadedOrPendingFilterByName(filter.type),
                    filterItem = self.addFilter(filterConfig, filter.name);
                if (filter.active)
                    filterItem.setActive(true);
            });
        },

        activeFilters: function() {
            return $.grep(this.filters, function(filter) {
                return filter.active && filter.config.isLoading !== true;
            });
        },

        defaultValues: function() {
            var values = {};
            $.each(this.filters, function(i, filter) {
                values[filter.name] = filter.config.defaultValues();
            });
            return values;
        },

        setSource: function(keyframe) {
            this.keyframe = keyframe;
            var source = keyframe.value;
            $.each(this.filters, function(i, filter) {
                if (!filter.active)
                    return;
                filter.setSource(source[filter.name]);
            });
        },

        onFilterValuesUpdated: function(filter, paramName) {
            this.fire("keyframeUpdated", [this.keyframe, filter, paramName]);
        },

        onFilterStateChanged: function(filter) {
            this.fire("filterStateChanged", [filter]);
        },

        onFilterConfigChanged: function(filter, configChanges) {
            this.fire("filterConfigChanged", [filter, configChanges]);
        },

        onFilterConfigRemoved: function(filter) {
            this.removeFilter(filter);
        }
    });
    

    Global.FilterList = FilterList;
})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function GitHub() {
        GitHub.$super.call(this);
        $(window).bind("message", this.onReceivedMesssage.bind(this));
    }
    
    Global.Utils.extend(GitHub).from(Global.EventDispatcher);

    $.extend(GitHub.prototype, {
        clientId: "9d5764d5b3fb3dbea0d6",

        url: "https://api.github.com",
        gistUrl: "https://gist.github.com",
        loginUrl: "https://github.com/login/oauth/authorize",
        tokenUrl: "https://github.com/login/oauth/access_token",

        postGist: function(access_token, id, publicGist, name, files, callback) {
            var xhr = new XMLHttpRequest(),
                data = {
                    "description": name,
                    "files": files
                },
                url = this.url + "/gists?client_id=" + encodeURIComponent(this.clientId),
                method = "POST";
            if (id) {
                url += "/" + encodeURIComponent(id);
                method = "PATCH";
            } else {
                data["public"] = publicGist;
            }
            if (access_token)
                url += "?access_token=" + encodeURIComponent(access_token);
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4)
                    return;
                var response = JSON.parse(xhr.responseText);
                callback(response);
            };

            xhr.open(method, url);
            xhr.send(JSON.stringify(data));
        },

        readGist: function(url, callback) {
            var id = this.extractGistIdFromUrl(url);
            if (!id)
                return;
            var xhr = new XMLHttpRequest(),
                requestUrl = this.url + "/gists/" + encodeURIComponent(id) + "?client_id=" + encodeURIComponent(this.clientId);
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4 || xhr.status != 200)
                    return;
                if (xhr.status == 400 || xhr.status == 404){
                    callback(xhr.status, null)
                    return;
                }
                var response = JSON.parse(xhr.responseText);
                callback(null, response);
            };
            xhr.open("GET", requestUrl);
            xhr.send(null);
        },

        _lastState: null,
        getLoginUrl: function() {
            this._lastState = Math.random();
            return this.loginUrl + "?client_id=" + encodeURIComponent(this.clientId) + "&scope=gist&state=" + encodeURIComponent(this._lastState);
        },

        onReceivedMesssage: function(event) {
            if (event.origin != location.origin)
                return;
            var data = event.data;
            if (data.state != this._lastState) {
                console.error("Invalid state read from github.")
                return;
            }
            if (!data.github.access_token) {
                this.fire("login", ["Invalid token"]);
                return;
            }
            this.fire("login", [null, data.github.access_token]);
        },

        // Extracts the gistId out of urls of the form:
        // https://gist.github.com/:userId/:gistId
        // https://gist.github.com/:userId/:gistId/ (A single trailing slash is allowed.)
        extractGistIdFromUrl: function(url)
        {
            var id = url.replace(new RegExp("^" + this.gistUrl + "/([^/]*)/([^/]*)/?$"), "$2");
            if (id == url) {
                // Nothing was extracted.
                return null;
            }
            return id;
        }
    });

    Global.GitHub = GitHub;


})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function Keyframe(filterList, time, value) {
        this.filterList = filterList;
        this.value = value;
        this.time = time;
        this.generated = false;
    }

    Keyframe.prototype = {
        makeGeneratedClone: function(time) {
            var keyframe = new Global.Keyframe(this.filterList, time, Global.Utils.clone(this.value));
            keyframe.generated = true;
            return keyframe;
        },

        blend: function(otherKeyframe, position, time) {
            var self = this,
                resultKeyframe = this.makeGeneratedClone(time),
                A = self.value,
                B = otherKeyframe.value,
                C = resultKeyframe.value;
            $.each(this.filterList.filters, function(index, filter) {
                if (!filter.active || filter.config.isLoading)
                    return;
                var filterName = filter.name;
                C[filterName] = filter.config.blendParams(A[filterName], B[filterName], position);
            });
            return resultKeyframe;
        },

        getFilterValues: function(filter) {
            if (!this.value.hasOwnProperty(filter.name))
                this.value[filter.name] = {};
            return this.value[filter.name];
        },

        addFilterDefaultValue: function(filter) {
            this.value[filter.name] = Global.Utils.clone(filter.config.defaultValues());
        },

        removeFilterValue: function(filter) {
            delete this.value[filter.name];
        },

        updateKeyframeAfterConfigChange: function(filter, configChange) {
            var filterValues = this.getFilterValues(filter);
            $.each(configChange.deletedKeys, function(i, name) {
                delete filterValues[name];
            });
            $.each(configChange.addedValues, function(name, value) {
                if (filterValues.hasOwnProperty(name))
                    return;
                filterValues[name] = value;
            });
            $.each(configChange.changedValues, function(name, value) {
                console.log("changed ", filter, name, value);
                filterValues[name] = value;
            });
        }
    };

    Global.Keyframe = Keyframe;


})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    // The following function will run in the worker thread.
    function WorkerInit() {
        /*global Module:true, self:true, allocate:true, cwrap:true, intArrayFromString:true, Runtime:true,
                 ALLOC_STACK:true, setValue:true, getValue:true, Pointer_stringify:true, Blob:true, webkitURL:true */
        /*jshint newcap:false, onecase:true */
        Module['print'] = function(str) {
            self.postMessage({type: "error", value: str});
        }

        var cwrap = Module['cwrap'];

        var Free = cwrap("free", "void", ["number"]);
        var ShCompile = cwrap("ShCompile", "number", ["number", "number", "number", "number"]);
        var ShConstructCompiler = cwrap("ShConstructCompiler", "number", ["number", "number", "number", "number"]);
        var ShFinalize = cwrap("ShFinalize", "number");
        var ShGetActiveUniform = cwrap("ShGetActiveUniform", "void", ["number", "number", "number", "number", "number", "number", "number"]);
        var ShGetInfo = cwrap("ShGetInfo", "void", ["number", "number", "number"]);
        var ShGetInfoLog = cwrap("ShGetInfoLog", "void", ["number", "number"]);
        var ShGetObjectCode = cwrap("ShGetObjectCode", "void", ["number", "number"]);
        var ShInitBuiltInResources = cwrap("ShInitBuiltInResources", "void", ["number"]);
        var ShInitialize = cwrap("ShInitialize");

        var SH_ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87;
        var SH_ACTIVE_UNIFORMS = 0x8B86;
        var SH_ATTRIBUTES_UNIFORMS = 0x0008;
        var SH_CSS_SHADERS_SPEC = 0x8B42
        var SH_ESSL_OUTPUT = 0x8B45;
        var SH_FRAGMENT_SHADER = 0x8B30;
        var SH_INFO_LOG_LENGTH = 0x8B84;
        var SH_INTERMEDIATE_TREE = 0x0002;
        var SH_JS_OUTPUT = 0x8B48;
        var SH_OBJECT_CODE = 0x0004;
        var SH_OBJECT_CODE_LENGTH = 0x8B88;
        var SH_VERTEX_SHADER = 0x8B31;
        var SH_WEBGL_SPEC = 0x8B41;

        var types = {
            0: "none",
            
            0x1406: "float",
            0x8B50: "vec2",
            0x8B51: "vec3",
            0x8B52: "vec4",
            
            0x1404: "int",
            0x8B53: "ivec2",
            0x8B54: "ivec3",
            0x8B55: "ivec4",

            0x8B56: "bool",
            0x8B57: "bool2",
            0x8B58: "bool3",
            0x8B59: "bool4",

            0x8B5A: "mat2",
            0x8B5B: "mat3",
            0x8B5C: "mat4",

            0x8B5E: "sampler2d"

            // 0x8B60: "sampler3d"

            // 0x8B63: "sampler2d_rect",
            // 0x8D66: "sampler2d_external"
        };

        ShInitialize();
        var builtinResources = allocate(100 * 4, 'i8'); // big enough to cover for the size of ShBuiltInResources
        ShInitBuiltInResources(builtinResources);

        var compilers = {
            "vertex": ShConstructCompiler(SH_VERTEX_SHADER, SH_CSS_SHADERS_SPEC, SH_JS_OUTPUT, builtinResources),
            "fragment": ShConstructCompiler(SH_FRAGMENT_SHADER, SH_CSS_SHADERS_SPEC, SH_JS_OUTPUT, builtinResources)
        };

        function compile(type, shaderString) {
            var stack = Runtime.stackSave();
            var compiler = compilers[type];

            var shaderStringPtr = allocate(intArrayFromString(shaderString), 'i8', ALLOC_STACK);
            var strings = allocate(4, 'i32', ALLOC_STACK);
            setValue(strings, shaderStringPtr, 'i32');
            var compileResult = ShCompile(compiler, strings, 1, SH_OBJECT_CODE | SH_ATTRIBUTES_UNIFORMS);
            
            var result = {
                original: shaderString,
                compileResult: compileResult,
                uniforms: []
            };

            ShGetInfo(compiler, SH_OBJECT_CODE_LENGTH, strings);
            var length = getValue(strings, 'i32');
            var shaderResultStringPtr = allocate(length, 'i8', ALLOC_STACK);
            ShGetObjectCode(compiler, shaderResultStringPtr);
            result.source = length > 1 ? Pointer_stringify(shaderResultStringPtr, length - 1) : "";
            
            ShGetInfo(compiler, SH_INFO_LOG_LENGTH, strings);
            length = getValue(strings, 'i32');
            shaderResultStringPtr = allocate(length, 'i8', ALLOC_STACK);
            ShGetInfoLog(compiler, shaderResultStringPtr);
            result.info = length > 1 ? Pointer_stringify(shaderResultStringPtr, length - 1) : "";

            ShGetInfo(compiler, SH_ACTIVE_UNIFORMS, strings);
            var uniformsCount = getValue(strings, 'i32');
            
            ShGetInfo(compiler, SH_ACTIVE_UNIFORM_MAX_LENGTH, strings);
            var maxUnfiromNameLength = getValue(strings, 'i32');
            var uniformName = allocate(maxUnfiromNameLength, 'i8', ALLOC_STACK);
            for (var i = 0; i < uniformsCount; ++i) {
                ShGetActiveUniform(compiler, i, strings, strings + 4, strings + 8, uniformName, 0);
                var uniformNameLength = getValue(strings, 'i32');
                var uniformType = getValue(strings + 8, 'i32');
                if (!types.hasOwnProperty(uniformType))
                    continue;
                result.uniforms.push({
                    size: getValue(strings + 4, 'i32'),
                    type: types[uniformType],
                    name: Pointer_stringify(uniformName, uniformNameLength)
                });
            }

            Runtime.stackRestore(stack);
            return result;
        }

        this.onmessage = function(event) {
            switch (event.data.type) {
                case "compile":
                    try {
                        var result = compile(event.data.shaderType, event.data.source);
                        self.postMessage({
                            type: "result",
                            result: result,
                            callback: event.data.callback
                        });
                    } catch (e) {
                        self.postMessage({
                            type: "error", 
                            error: e ? e.toString() : "Undefined error",
                            callback: event.data.callback
                        });
                    }
                break;
                default:
                    self.postMessage({
                        type: "error", 
                        error: "Unrecognized command",
                        callback: event.data.callback
                    });
                    break;
            }
        }
        self.postMessage({type: 'loaded'});
    }

    function AngleLib() {
        AngleLib.$super.call(this);
        this.lastCallbackId = 0;
        this.callbacks = {};
        this.queue = [];
    }

    Global.Utils.extend(AngleLib).from(Global.EventDispatcher);

    $.extend(AngleLib.prototype, {
        angleJS: "third_party/angle/angle.closure.js",

        load: function() {
            if (this.loadStarted) 
                return;
            this.loadStarted = true;
            var self = this;
            var xhr = new XMLHttpRequest();
            xhr.onprogress = function(ev) {
                self.fire("progress", [ev.loaded / ev.total]);
            }
            xhr.onreadystatechange = function(ev) {
                if (xhr.readyState != 4 || xhr.status != 200)
                    return;
                var blob = new Blob([xhr.response, "\n(", WorkerInit.toString(), ")();"]);
                self.worker = new Worker(webkitURL.createObjectURL(blob));
                self.worker.onmessage = function(ev) {
                    self.onWorkerMessage(ev);
                }
                // Start the worker.
                self.worker.postMessage("");
                self.fire("completed");
            };
            xhr.open("GET", this.angleJS);
            xhr.send();
        },

        onWorkerMessage: function(ev) {
            switch (ev.data.type) {
                case 'result':
                    this.onWorkerResult(ev.data);
                    break;
                case 'loaded':
                    this.onWorkerLoaded();
                    break;
                case 'error':
                    this.onWorkerError(ev.data);
                    break;
            }
        },

        parseErrors: function(errorsString) {
            var errors = [],
                match,
                parser = /^(\w*):\s(\d*):(.*)/gm;
            while((match = parser.exec(errorsString)) !== null) {
                errors.push({
                    type: match[1],
                    index: parseInt(match[2], 10),
                    error: match[3]
                });
            }
            return errors;
        },

        onWorkerResult: function(data) {
            var callback = this._getCallback(data.callback);
            if (!callback)
                return;
            var result = data.result;
            if (result.info && result.info.length)
                result.errors = this.parseErrors(result.info);
            callback(null, result);
        },

        onWorkerLoaded: function(data) {
            this.loaded = true;
            var queue = this.queue,
                self = this;
            this.queue = null;
            $.each(queue, function(i, item) {
                self.compile(item.shaderType, item.source, item.callback);
            });
        },

        onWorkerError: function(data) {
            console.log(data);
            var callback = this._getCallback(data.callback);
            if (!callback)
                return;
            callback(data.error);
        },

        _registerCallback: function(callback) {
            var id = ++this.lastCallbackId;
            this.callbacks[id] = callback;
            return id;
        },

        _getCallback: function(id) {
            var callback = this.callbacks[id];
            delete this.callbacks[id];
            return callback;
        },

        compile: function(type, source, callback) {
            if (!this.loaded) {
                this.queue.push({
                    shaderType: type,
                    source: source,
                    callback: callback
                });
                if (!this.loadStarted)
                    this.load();
                return;
            }
            this.worker.postMessage({
                type: "compile", 
                shaderType: type,
                source: source, 
                callback: this._registerCallback(callback)
            });
        }
    });

    Global.AngleLib = AngleLib;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function roundedValue(value) {
        if (typeof value == "number")
           return Math.round(value * 1000) / 1000;
       return value;
    }

    var noColoring = {
        parameterName: Global.Utils.identity,
        value: function(value, unit) {
            return roundedValue(value) + ((unit !== undefined) ? unit : "");
        },
        fn: function(fn, args) {
            var valueArgs = [];
            $.each(args, function(key, value) { valueArgs.push(noColoring.value(value)); });
            return fn + "(" + valueArgs.join(", ") + ")";
        },
        builtinFn: function(fn, args) {
            var valueArgs = [];
            $.each(args, function(key, value) { valueArgs.push(noColoring.value(value)); });
            return fn + "(" + valueArgs.join(" ") + ")";
        },
        uri: Global.Utils.identity,
        keyword: Global.Utils.identity,
        keyframe: function(percent, filters) {
            return percent + " {\n" +
                   "    -webkit-filter: " + filters.join(" ") + ";\n" +
                   "}";
        }
    };
     
    var colorTheme = {
        parameterName: function(name) {
            return "<span class='parameter-name'>" + name + "</span>";
        },
        value: function(value, unit) {
            return "<span class='parameter-value'>" + roundedValue(value) + "</span>" +
                    ((unit !== undefined) ? ("<span class='parameter-unit'>" + unit + "</span>") : "");
        },
        fn: function(fn, args) {
            var valueArgs = [];
            $.each(args, function(key, value) { valueArgs.push(colorTheme.value(value)); });
            return "<span class='function-name " + fn + "'>" + fn +
                 "</span><span class='function-parenthesis left " + fn + "'>(</span><span class='function-arguments " + fn + "'>" +
                 valueArgs.join(", ") + "</span><span class='function-parenthesis right " + fn + "'>)</span>";
        },
        builtinFn: function(fn, args) {
            var valueArgs = [];
            $.each(args, function(key, value) { valueArgs.push(colorTheme.value(value)); });
            return "<span class='function-name " + fn + "'>" + fn +
                 "</span><span class='function-parenthesis left " + fn + "'>(</span><span class='function-arguments " + fn + "'>" +
                 valueArgs.join(" ") + "</span><span class='function-parenthesis right " + fn + "'>)</span>";
        },
        _dataURL: /^data:/,
        uri: function(uri) {
            var className = "";
            if (this._dataURL.test(uri))
                className = " data-uri";
            return "<a class='uri" + className + "' href='" + uri + "' target='_blank'>" + uri + "</a>";
        },
        keyword: function(value) {
            return "<span class='keyword'>" + value + "</span>";
        },
        keyframe: function(percent, filters) {
            return "<div class='keyframe'>"+
                   "<span class='keyframe-percent'>" + percent + "</span> " +
                   "<span class='curly-brace left'>{</span>" + 
                   "<div class='keyframe-content'>" + 
                   "<span class='property-name'>-webkit-filter</span>: <span class='property-value'>" +
                   filters.join(" ") +
                   "</span>;</div><span class='curly-brace right'>}</span></div>";
        }
    };

    Global.ColorSchemes = {
        noColoring: noColoring,
        colorTheme: colorTheme
    };

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function Config() {
    }
    Global.Config = Config;

    Config.prototype.load = function(config) {
        this.filters = config;
    }

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    Global.CSSGenerators = {};

    Global.CSSGenerators.identity = Global.Utils.identity;

    Global.CSSGenerators.transform = function(v, colors) {
        return [
        colors.fn("perspective", [colors.value(Global.Utils.checkDefaultNumber(v.perspective, 1000))]),
        colors.fn("scale", [colors.value(Global.Utils.checkDefaultNumber(v.scale, 1))]),
        colors.fn("rotateX", [colors.value(Global.Utils.checkDefaultNumber(v.rotationX, 0), "deg")]),
        colors.fn("rotateY", [colors.value(Global.Utils.checkDefaultNumber(v.rotationY, 0), "deg")]),
        colors.fn("rotateZ", [colors.value(Global.Utils.checkDefaultNumber(v.rotationZ, 0), "deg")])].join(" ");
    }
     
    Global.CSSGenerators.filterArray = function(values, colors) {
       return colors.fn("array", values);
    }
        
    Global.CSSGenerators.vector = function(values, colors) {
        return $.map(values, function(val) { return colors.value(val); }).join(" ");
    }

    Global.CSSGenerators.color = function(values, colors) {
        return colors.fn("rgba", $.map(values, function(val, index) { 
            if (index < 3)
                return colors.value(Math.round(val * 255));
            return colors.value(val); 
        }));
    }

    Global.CSSGenerators.warpArray = function(values, colors) {
        return Global.WarpHelpers.generateWarpArray(values, colors);
    }

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

	function FileEntry(storage, path, name) {
		this.storage = storage;
		this.path = path;
		this.name = name;
	}

	FileEntry.prototype = {
		toURL: function(value) {
			return "data:text/plain;base64," + Global.Utils.encodeBase64(value || "");
		}
	};
	
	function LocalStorage() {
		this.storage = window.localStorage;
		this.files = this.readFileSystem();
	}

	LocalStorage.prototype = {
		descriptor: "filesystem",

		register: function(callback) {
			callback(this);
		},

		saveFileSystem: function() {
			if (!this.storage)
				return;
			this.storage.setItem(this.descriptor, JSON.stringify(this.files));
		},

		readFileSystem: function() {
			if (!this.storage)
				return null;
			var fileSystem = this.storage.getItem(this.descriptor);
			if (!fileSystem)
				return {
					root: {},
					filesCount: 0
				};
			return JSON.parse(fileSystem);
		},

		getDirectoryEntry: function(path, create) {
			if (!this.files)
				return;
			var entry = this.files.root;
			for (var i = 0; i < path.length; ++i) {
				var folderName = path[i];
				if (entry.hasOwnProperty(folderName)) {
					entry = entry[folderName];
					continue;
				}
				if (!create)
					return null;
				entry = entry[folderName] = {};
			}
			return entry;
		},

		splitPath: function(path) {
			return path.split("/");
		},

		saveFileEntry: function(entry, value) {
			if (!this.storage)
				return;
			var filePath = entry.path,
				fileName = entry.name,
				dirEntry = this.getDirectoryEntry(filePath, true);
			var key = dirEntry[fileName];
			if (!key) {
				var fileId = "file" + (++this.files.filesCount);
				key = dirEntry[fileName] = {
					fileId: fileId,
					name: fileName
				};
				this.saveFileSystem();
			}
			this.storage.setItem(key.fileId, value);
		},

		removeFileEntry: function(entry, value) {
			if (!this.storage)
				return;
			var filePath = entry.path,
				fileName = entry.name,
				dirEntry = this.getDirectoryEntry(filePath, true);
			var key = dirEntry[fileName];
			if (!key)
				return;
			delete dirEntry[fileName];
			this.saveFileSystem();
			this.storage.removeItem(key);
		},

		loadFileEntry: function(entry) {
			if (!this.storage)
				return null;
			var filePath = entry.path,
				fileName = entry.name,
				dirEntry = this.getDirectoryEntry(filePath, false);
			if (!dirEntry)
				return null;
			var key = dirEntry[fileName];
			if (!key)
				return null;
			return this.storage.getItem(key.fileId);
		},

		get: function(path, callback) {
			var self = this;
			this.getEntry(path,
				function(err, entry) {
					if (err) {
						callback(err);
						return;
					}
					self.readFile(entry, callback);
				});
		},

		getEntry: function(path, callback) {
			var filePath = this.splitPath(path),
				fileName = filePath.pop();
			callback(null, new FileEntry(this, filePath, fileName));
		},

		readFile: function(entry, callback) {
			callback(null, this.loadFileEntry(entry));
		},

		save: function(path, data, callback) {
			var self = this;
			this.getEntry(path, function(err, entry) {
				self.saveFileEntry(entry, data);
				if (callback)
					callback(null, entry);
			});
		},

		list: function(path, callback) {
			var filePath = this.splitPath(path),
				dirEntry = this.getDirectoryEntry(filePath, true),
				list = [],
				self = this;
			if (!dirEntry)
				return callback(null, []);
			$.each(dirEntry, function(i, entry) {
				list.push(new FileEntry(self, filePath, entry.name));
			});
			callback(null, list);
		},

		createDirectory: function(path, callback) {
			var filePath = this.splitPath(path),
				dirEntry = this.getDirectoryEntry(filePath, true);
			callback(null, path);
		},

		deleteFile: function(path, callback) {
			var self = this;
			this.getEntry(path, function(err, entry) {
				self.removeFileEntry(entry);
				callback(null);
			});
		}
	}

	Global.LocalStorage = LocalStorage;
})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){

    var mixers = {
        mixNumber: function(a, b, t) {
            return a * (1 - t) + b * t;
        },
        
        mixVector: function(a, b, t) {
            var r = [];
            for (var i=0; i<a.length; ++i)
                r.push(mixers.mixNumber(a[i], b[i], t));
            return r;
        },
        
        mixVectorOfVectors: function(a, b, t) {
            var r = [];
            for (var i=0; i < a.length; ++i) {
                var row = []; r.push(row);
                var l2 = a[i].length;
                for (var j=0; j < l2; ++j)
                    row.push(Global.mixers.mixVector(a[i][j], b[i][j], t));
            }
            return r;
        },
        
        mixHash: function(fn) {
            var mixFn = Global.mixers[fn];
            return function(a, b, t) {
                var r = {};
                $.each(a, function(key, valueA) {
                    r[key] = mixFn(valueA, b[key], t);
                });
                return r;
            }
        },
        
        dontMix: function(a, b, t) {
            return a;
        }
    };

    Global.mixers = mixers;
})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
	
	function Timer(timeout) {
		Timer.$super.call(this);
		this.timeout = timeout;
		this.args = null;
		this.timer = null;
		this.timerCallback = this.onTimerFired.bind(this);
	}

	Global.Utils.extend(Timer).from(Global.EventDispatcher);

	$.extend(Timer.prototype, {
		invoke: function(args) {
			this.args = args;
			this.clearTimer();
			this.installTimer();
		},

		clearTimer: function() {
			clearTimeout(this.timer);
			this.timer = null;
		},

		installTimer: function() {
			this.timer = setTimeout(this.timerCallback, this.timeout);
		},

		onTimerFired: function() {
			this.timer = null;
			this.fire("timerFired", this.args);
		}
	});

	Global.Timer = Timer;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function () {
    
    var factors = {};
    function factor(n) {
        if (factors[n])
            return factors[n];
        var result = 1;
        for (var i = 2; i <= n; ++i)
            result *= i;
        factors[n] = result;
        return result;
    }

    function binomialCoefficient(n, i) {
        return factor(n) / (factor(i) * factor(n-i));
    }

    function calculateB(i, n, u) {
        var bc = binomialCoefficient(n, i);
        return bc * Math.pow(u, i) * Math.pow(1 - u, n - i);
    }
    function calculate(u, v, n, m, k) {
        var resultX = 0;
        var resultY = 0;
        var resultZ = 0;
        for (var i = 0; i <= n; ++i)
            for (var j = 0; j <= m; ++j) {
                var c = calculateB(i, n, u) * calculateB(j, m, v);
                resultX += c * k[i][j][0];
                resultY += c * k[i][j][1];
                resultZ += c * k[i][j][2];
            }
        return [resultX, resultY, resultZ];
    }
    
    function draw(el, width, height, k, currentPointCoords, currentHoverPointCoors) {
        var c = el.getContext("2d");
        c.save();

        // Background layer
        c.clearRect(0, 0, width, height);
        
        // Mesh layer
        c.lineWidth = 1;
        c.strokeStyle = "#414141";
    
        c.beginPath();

        var i, j, first, p;

        var n = k.length - 1, m = k[0].length - 1;
        for (i = 0; i < 1; i += 0.1) {
            first = true;
            for (j = 0; j < 1; j += 0.1) {
                p = calculate(i, j, n, m, k);
                if (first)
                    c.moveTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                else
                    c.lineTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                first = false;
            }
        }
    
        for (j = 0; j < 1; j += 0.1) {
            first = true;
            for (i = 0; i < 1; i += 0.1) {                    
                p = calculate(i, j, n, m, k);
                if (first)
                    c.moveTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                else
                    c.lineTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                first = false;
            }
        }
        c.stroke();

        // Control points mesh
    
        c.beginPath();
        c.strokeStyle = "#9c9c9c";
        for (i = 0; i <= n; ++i) {
            for (j = 0; j <= m; ++j) {
                p = k[i][j];
                if (!j)
                    c.moveTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                else
                    c.lineTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
            }
        }
        for (j = 0; j <= m; ++j) {
            for (i = 0; i <= n; ++i) {
                p = k[i][j];
                if (!i)
                    c.moveTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                else
                    c.lineTo(toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
            }
        }
        c.stroke();
        
        // Control points
        var gradient = c.createLinearGradient(0, 0, 0, 5);
        gradient.addColorStop(0, "#b2b2b2");
        gradient.addColorStop(1, "#8d8d8d");

        c.fillStyle = gradient;
        c.strokeStyle = "black";
        c.lineWidth = 1;
        c.shadowColor = "#0088ff";
        c.shadowBlur = 0;

        for (i = 0; i <= n; ++i) {
            for (j = 0; j <= m; ++j) {
                p = k[i][j];
                if (currentPointCoords && i == currentPointCoords.i && j == currentPointCoords.j) {
                    c.shadowBlur = 15;
                    c.shadowColor = "#9988ff";
                } else if (currentHoverPointCoors && i == currentHoverPointCoors.i && j == currentHoverPointCoors.j) {
                    c.shadowBlur = 10;
                    c.shadowColor = "#0088ff";
                } else
                    c.shadowBlur = 0;

                var size = (p[2] + 1000) / 300 + 2;
                c.setTransform(1, 0, 0, 1, toCanvasCoord(p[0], width), toCanvasCoord(p[1], height));
                c.beginPath();
                c.arc(0, 0, size, 0, Math.PI*2, true);
                c.fillStyle = "rgba(255, 255, 255, 0.9)";
                c.fill();

                c.strokeStyle = "black";
                c.stroke();

                c.beginPath();
                c.arc(0, 0, size - 1.5, 0, Math.PI*2, true);
                c.fillStyle = gradient;
                c.fill();
            }
        }
    
        c.restore();
    }
    
    var canvasMargins = 40;
    
    function toCanvasCoord(x, w) {
        return (x + 0.5) * (w - canvasMargins) + canvasMargins / 2;
    }
    
    function fromCanvasCoord(x, w) {
        return (x - canvasMargins / 2) / (w - canvasMargins) - 0.5;
    }
    
    function pointIsNearMouse(p, x, y, w, h) {
        var pointSize = 15;
        return (toCanvasCoord(p[0], w) - pointSize) < x && (toCanvasCoord(p[0], w) + pointSize) > x && 
                (toCanvasCoord(p[1], h) - pointSize) < y && (toCanvasCoord(p[1], h) + pointSize) > y;
    }

    function findPoint(k, x, y, coords, w, h) {
        var n = k.length - 1, m = k[0].length - 1;
        for (var i = 0; i <= n; ++i)
            for (var j = 0; j <= m; ++j)
                if (pointIsNearMouse(k[i][j], x, y, w, h)) {
                    if (coords) {
                        coords.i = i;
                        coords.j = j;
                    }
                    return k[i][j];
                }
        return null;
    }
    
    function buildWarpCanvas(el, width, height, updateCallback, zslider) {
        var parent = $(el).css({
            "position": "relative",
            "width": width + "px",
            "height": height + "px"
        });  
        
        if (!zslider) {
            zslider = $("<input type='range' class='vertical' min='-1000' max='1000' value='0' />")
                      .appendTo(parent)
                      .css({
                            "display": "none",
                            "position": "absolute",
                            "top":  "-4px",
                            "left": (width + 10) + "px",   
                            "width": height + "px"
                      });
        }
        
        var canvas = $("<canvas />")
                    .appendTo(parent)
                    .addClass("warp-control")
                    .attr({
                        "width": width,
                        "height": height
                     })
                    .css({
                        "position": "absolute", 
                        "left": "0",
                        "top": "0"
                    })
        
        var currentPointCoords = {i:-1, j:-1};
        var currentPoint = null;
        var dragging = false;
        
        var pointsRef = null;
        
        function hasPoints() {
            return !!pointsRef;
        }
        
        function points() {
            return pointsRef;
        }
        
        function setPoints(points) {
            if (points === pointsRef)
                return;
            currentPoint = null;
            currentPointCoords = {i:-1, j:-1};
            updateZSlider();
            redraw();
            return pointsRef = points;
        }
        
        function redraw(pointCoords) {
            if (!hasPoints()) return;
            draw(canvas[0], width, height, points(), pointCoords, currentPointCoords);
        }
        
        var startX, startY, pageX, pageY;
        canvas.mousedown(function(e) {
            e.preventDefault();
            if (!hasPoints()) return;
            currentPoint = findPoint(points(), event.offsetX, event.offsetY, currentPointCoords, width, height);
            startX = event.offsetX; pageX = event.pageX;
            startY = event.offsetY; pageY = event.pageY;
            redraw();
            updateZSlider();
            dragging = currentPoint != null;
            document.addEventListener("mousemove", mouseMove, true);
            document.addEventListener("mouseup", mouseUp, true);
        });

        function mouseMove(e) {
            e.preventDefault();
            if (!hasPoints()) return;
            var offsetX = Math.min(width, Math.max(0, event.pageX - pageX + startX)),
                offsetY = Math.min(height, Math.max(0, event.pageY - pageY + startY));
            currentPoint[0] = fromCanvasCoord(offsetX, width);
            currentPoint[1] = fromCanvasCoord(offsetY, height);
            redraw();
            updateCallback();
        }

        canvas.mousemove("mousemove", function(e) {
            if (dragging && currentPoint)
                return;
            var selectPointCoords = {i:-1, j:-1};
            var point = findPoint(points(), event.offsetX, event.offsetY, selectPointCoords, width, height);
            redraw(point ? selectPointCoords : null);
        });
        
        function mouseUp(e){
            document.removeEventListener("mousemove", mouseMove, true);
            document.removeEventListener("mouseup", mouseUp, true);
            e.preventDefault();
            dragging = false;
            redraw();
        }
        
        zslider.change(function() {
            if (currentPoint) {
                currentPoint[2] = parseFloat(zslider.attr("value"));
                redraw();
                updateCallback();
            }
        });
    
        function updateZSlider() {
            if (!currentPoint) {
                zslider.attr("disabled", "disabled").hide();
            } else {
                zslider.removeAttr("disabled").show();
                zslider.attr("value", currentPoint[2]);
            }
        }
        
        redraw();
        
        return {
            redraw: redraw,
            hasPoints: hasPoints,
            points: points,
            setPoints: setPoints
        };
    }
    
    function generateWarpArray(points, colors) {
        var controlPoints = [];
        for (var i = 0; i < points.length; ++i) {
            var l = points[i].length;
            for (var j = 0; j < l; ++j) {
                var p = points[i][j];
                controlPoints.push(colors.value(p[0]));
                controlPoints.push(colors.value(p[1]));
                controlPoints.push(colors.value(p[2]));
            }
        }
        return colors.fn("array", controlPoints);
    }
    
    function generateWarpPoints() {
        var l = -0.5,
            t = -0.5,
            w = 1,
            h = 1; // height

        var countX = 4;
        var countY = 4;
    
        var hW = w  / (countX - 1);
        var hH = h / (countY - 1);
    
        var k = [];
        for (var j = 0; j < countY; ++j) {
            var row = []; 
            for (var i = 0; i < countX; ++i)
                row.push([i * hW + l, j * hH + t, 0]);
            k.push(row);
        }
        
        return k;
    }
    
    Global.WarpHelpers = {
        buildWarpCanvas: buildWarpCanvas,
        generateWarpArray: generateWarpArray,
        generateWarpPoints: generateWarpPoints
    };
    
})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    function FilterStoreView(filterStore, filterList, filterListView, shaderEditor, type) {
        FilterStoreView.$super.call(this);
        
        this.type = this.filterTypes[type];
        this.filterStore = filterStore;
        this.filterStore.on("filterAdded", this.onFilterAdded.bind(this));
        this.filterStore.on("filterDeleted", this.onFilterDeleted.bind(this));
        
        this.filterList = filterList;
        this.filterListView = filterListView;
        this.shaderEditor = shaderEditor; 

        this.dockPanel = new Global.DockPanel(this.type.name);
        
        this.filterStockListEl = $("<div />").addClass("filter-stock-list");
        this.filterStockEl = $("<div />")
                    .addClass("filter-stock")
                    .append(this.filterStockListEl)
                    .appendTo(this.dockPanel.el);

        this.filters = []; 

    }
      
    Global.Utils.extend(FilterStoreView).from(Global.EventDispatcher);
    
    $.extend(FilterStoreView.prototype, {

        filterTypes: {
            "builtins": {
                name: "Built-in",
                check: function(filterConfig) {
                    return filterConfig.isBuiltin;
                }
            },
            "custom": {
                name: "Custom",
                check: function(filterConfig) {
                    return !filterConfig.isBuiltin && !filterConfig.isFork;
                }
            },
            "forked": {
                name: "Forked Custom",
                check: function(filterConfig) {
                    return !filterConfig.isBuiltin && filterConfig.isFork;
                }
            }
        },

        onFilterAdded: function(filterConfig, loadedFromPreset) {
            if (!this.type.check(filterConfig))
                return;                
                
            function addFilter(e){
                e.preventDefault()
                self.filterListView.dockPanel.setActive(true);
                var filter = self.filterList.addFilter(filterConfig);
                filter.setActive(true);
                self.fire("filterSelected", [filter]);
            }

            var self = this;
            var el = $("<div />")
                    .addClass("filter-item")
                    .addClass(filterConfig.isBuiltin ? "builtin-filter" : "custom-filter")
                    .data("filterConfig", filterConfig)
                    .attr("data-filterName", encodeURIComponent(filterConfig.name))
                    .click(addFilter);
            
            if (filterConfig.isFork)
                el.addClass("forked-filter");

            var cssPreview = filterConfig.generatePreviewCode(),
                previewEl = $("<div />")
                            .addClass("filter-preview")
                            .css("-webkit-filter", cssPreview)
                            .appendTo(el);

            var titleEl = $("<div />")
                            .addClass("filter-label")
                            .text(filterConfig.label)
                            .appendTo(el);
                             
            var buttonBox = $("<div class='button-box'>");
            el.append(buttonBox)
                
            if (!filterConfig.isBuiltin) { 
                if (!filterConfig.isFork) {
                    buttonBox.append($("<a href='#' />")
                        .addClass("customize-link icon icon-fork")
                        .text("Fork")
                        .attr("title", "Fork filter")
                        .click(function(e) {
                            e.stopPropagation();
                            e.preventDefault();
                            self.forkFilter(filterConfig);
                        }));
                } else {
                    buttonBox.append($("<a href='#' />")
                        .addClass("customize-link icon icon-edit")
                        .text("Edit")
                        .attr("title", "Edit filter")
                        .click(function(e) {
                            e.stopPropagation();  
                            e.preventDefault();
                            self.customizeFilter(el, filterConfig);
                        }));
                    buttonBox.append($("<a href='#'/>")
                        .addClass("icon icon-remove")
                        .text("Delete")
                        .attr("title", "Delete filter")
                        .click(function(e) {
                            e.stopPropagation();
                            e.preventDefault();
                            var message = "Are you sure you want to delete the '"+ filterConfig.label +"' filter?";
                            
                            if(window.confirm(message))
                                self.deleteFilter(filterConfig);
                        }));
                }
            }

            filterConfig.on("configChanged", this.onFilterConfigChanged.bind(this, filterConfig, titleEl, previewEl));

            this.insertFilterElement(el, filterConfig);

            if (filterConfig.isFork && !loadedFromPreset) {
                this.dockPanel.setActive(true);
                el[0].scrollIntoView();
                el.hide().effect("highlight", {}, 400);
                this.customizeFilter(el, filterConfig);
                var filter = self.filterList.addFilter(filterConfig);
                filter.setActive(true);
            }
        },
        
        onFilterDeleted: function(filterConfig){  
            this.filters.forEach(function(filter, index){
                if (filter.data("filterConfig").name === filterConfig.name){
                    filter.remove()
                    this.filters.splice(index, 1)
                }
            }.bind(this))
            
            if (this.shaderEditor.filter && this.shaderEditor.filter.name === filterConfig.name){
                this.shaderEditor.hide()
            }
        },

        insertFilterElement: function(el, filterConfig) {
            this.filters.push(el);

            this.filters.sort(function(elA, elB) {
                var a = elA.data("filterConfig"),
                    b = elB.data("filterConfig");
                return a.name.localeCompare(b.name);
            });

            var index = this.filters.indexOf(el);
            if (index == this.filters.length - 1)
                this.filterStockListEl.append(el);
            else
                this.filters[index + 1].before(el);
        },

        forkFilter: function(filterConfig) {
            this.filterStore.forkFilter(filterConfig);
        },
        
        deleteFilter: function(filterConfig){
            this.filterStore.deleteFilter(filterConfig)
        },

        customizeFilter: function(el, filterConfig) {
            $.each(this.filters, function(i, el) { el.removeClass("current"); });
            el.addClass("current");
            this.shaderEditor.loadFilter(filterConfig);
        },

        onFilterConfigChanged: function(filterConfig, titleEl, previewEl) {
            titleEl.text(filterConfig.label);
            previewEl.css("-webkit-filter", filterConfig.generatePreviewCode());
        },

        insertUnsupportedPopup: function(el) {
            this.unsupportedPopupEl = el.clone(true);
            this.filterStockListEl.before(this.unsupportedPopupEl);
        }
    });

    Global.FilterStoreView = FilterStoreView;
})();

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function ActiveFilterListView(filterList) {
        this.filterList = filterList;
        this.filterList.on("filterAdded", this.onFilterAdded.bind(this));
        this.filterList.on("filterRemoved", this.onFilterRemoved.bind(this));
          
        this.filterMessageEl = $("#filter-list-empty");

        this.container = $("#filter-list");

        this.dockPanel = new Global.DockPanel("Active Filters");
        this.scrollContainer = $("#active-filters").appendTo(this.dockPanel.el);

        this.filterItemsViewsByName = {};

        this.makeDragable();
        this.updateFilterCount();
    }

    ActiveFilterListView.prototype = {
        updateFilterCount: function() {
            this.filterMessageEl.toggle(!this.filterList.filters.length);
        },

        makeDragable: function() {
            var self = this;
            this.scrollContainer.sortable({
                distance: 15,
                axis: 'y',
                items: 'li',
                handle: '.dragpoint',
                scroll: true, 
                start: function(event, ui) {
                    ui.helper.addClass("dragging");
                },
                beforeStop: function(event, ui) {
                    ui.helper.removeClass("dragging");
                },            
                update: function(event, ui) {
                    $(this).find("li").each(function (index, el) {
                        var filterItemView = $(el).data("filterItemView");
                        if (filterItemView)
                            filterItemView.filter.order = index;
                    });
                    self.filterList.updateFiltersOrder();
                }
            });
        },

        onFilterAdded: function(filter) {
            var itemView = new Global.FilterItemView(filter);
            this.filterItemsViewsByName["_" + filter.name] = itemView;
            this.container.append(itemView.el);
            this.updateFilterCount();
            this.scrollContainer.scrollTop(this.container.outerHeight() + 1000);
        },

        onFilterRemoved: function(filter, useAnimation) {
            var self = this,
                filterItemView = this.filterItemsViewsByName["_" + filter.name];
            if (!filterItemView)
                return;
            if (useAnimation) {
                filterItemView.el.slideUp(100, function() {
                    filterItemView.el.remove();
                    self.updateFilterCount();
                });
            } else {
                filterItemView.el.remove();
                this.updateFilterCount();
            }
        }
    };

    Global.ActiveFilterListView = ActiveFilterListView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function CSSCodeView(animation) {
        this.animation = animation;
        this.dockPanelCode = new Global.DockPanel("CSS Syntax");
        this.dockPanelAnimationCode = new Global.DockPanel("CSS Animation Syntax");
        this.filterCodeEl = $("#filter-code").appendTo(this.dockPanelCode.el).click(this.selectCode.bind(this));
        this.animationCodeEl = $("#animation-code").appendTo(this.dockPanelAnimationCode.el).click(this.selectCode.bind(this));
        this.animation.on("filtersUpdated", this.onFiltersUpdated.bind(this));
        this.lastFilterCodeHtml = null;
        this.lastAnimationCodeHtml = null;
    }

    CSSCodeView.prototype = {
        NoFiltersAddedText: "No filters applied. Add a filter from the left panel to see the CSS syntax for it.",
        NoAnimationAddedText: "No keyframes applied. Add a filter from the left panel and change the values to see the CSS syntax for it.",

        onFiltersUpdated: function(cssFilters, filterCodeHtml, animationCodeHtml) {
            if (!filterCodeHtml.length)
                filterCodeHtml = this.NoFiltersAddedText;
            if (!animationCodeHtml.length)
                animationCodeHtml = this.NoAnimationAddedText;

            if (filterCodeHtml != this.lastFilterCodeHtml) {
                this.filterCodeEl.html(filterCodeHtml);
                this.lastFilterCodeHtml = filterCodeHtml;
            }
            if (animationCodeHtml != this.lastAnimationCodeHtml) {
                this.animationCodeEl.html(animationCodeHtml);
                this.lastAnimationCodeHtml = animationCodeHtml;
            }
        },

        selectCode: function(ev) {
            document.getSelection().selectAllChildren(ev.currentTarget);
        }
    }

    Global.CSSCodeView = CSSCodeView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(){
    
    function FilterItemView(filter) {
        this.filter = filter;
        this.filter.on("filterStateChanged", this.onFilterStateChanged.bind(this));
        this.filter.on("filterSourceChanged", this.onFilterSourceChanged.bind(this));
        this.filter.config.on("configChanged", this.onFilterConfigChanged.bind(this));
        this.init();
    }

    FilterItemView.prototype = {
        init: function() {
            var self = this;

            this._buildParamsControls();

            this.el = (this.el || $("<li />"))
                .toggleClass("builtin-filter", this.filter.isBuiltin())
                .toggleClass("custom-filter", !this.filter.isBuiltin())
                .toggleClass("forked-filter", this.filter.isFork())
                .toggleClass('current', this.filter.active)
                .toggleClass('loading', !!this.filter.config.isLoading)
                .data("filterItemView", this);

            if (!this.deleteButton) {
                this.deleteButton = $("<button class='delete-filter' />").text("Delete").appendTo(this.el)
                    .click(function() {
                       self.filter.removeFilter(); 
                    });
            }

            if (!this.dragPoint)
                this.dragPoint = $("<div class='dragpoint' />").appendTo(this.el);
                
            this.stateCheckbox = $("<input type='checkbox'>")
                .click(function(e) {
                    self.filter.toggleFilter();
                });
                
            if (!this.stateToggle)
                this.stateToggle = $("<label class='slide-toggle'>")
                    .append(this.stateCheckbox)
                    .append($("<div>")
                       .append($("<span>"))
                    ) 
                    .appendTo(this.el)
                     
            if (!this.labelEl) {
                this.labelEl = $("<div class='filter-name' />")
                    .appendTo(this.el)
            }
            
            this.labelEl.text(this.filter.config.label);
            
            if (!this.configEl)
                this.configEl = $("<div class='config' />").appendTo(this.el);
            this.configEl.empty().append(this.controlsEl);
        },

        onFilterConfigChanged: function() {
            this.init();
        },

        onFilterStateChanged: function(active) {
            if (active) {
                this.el.addClass('current');
                this.stateCheckbox.attr("checked", true)
                this.configEl.hide().slideDown(100);
            } else {
                this.el.removeClass('current');
                this.stateCheckbox.attr("checked", false)
                this.configEl.slideUp(100);
            }
        },

        /**
         * Builds shader controls for the given set of parameter values
         * and description
         */
        _buildParamsControls: function () {
            var table = $("<table class='paramsTable' />"),
                self = this,
                controls = [],
                config = this.filter.config;
            this.controls = controls;
            this.controlsEl = table;
            if (config.isLoading)
                return;
            $.each(config.params, function (name) {
                var filterParam = config.config[name],
                    type = filterParam.type || 'range';
                if (type == 'hidden' || type == "unknown")
                    return;

                var EditorClass = Global.Controls.get(type);
                if (!EditorClass)
                    return;
                var editor = new EditorClass(self, name, filterParam);
                controls.push({
                    name: name,
                    editor: editor
                });
                var tr = $("<tr class='field' />").appendTo(table).append("<td class='field-label'>" + name + ":</td");
                editor.pushControls(tr);
            });
            if (this.filter.source)
                this.onFilterSourceChanged(this.filter.source);
        },

        valuesUpdated: function(paramName) {
            this.filter.valuesUpdated(paramName);
        },

        onFilterSourceChanged: function(source) {
            $.each(this.controls, function(i, control) {
                control.editor.setSource(source);
            });
        }
    };

    Global.FilterItemView = FilterItemView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function PresetStoreView(presetStore) {
        this.presetStore = presetStore;
        this.dockPanel = new Global.DockPanel("Presets");
        
        this.activeAnimationEl = $("#active-animation");
        this.animationsEl = $("#animations");
        this.saveAsEl = $("#save-animation");

        this.animationsContainerEl = $("#animations-container").appendTo(this.dockPanel.el);
        
        this.presetsByName = {};
        
        this.outsideLayerEl = $("<div />").addClass("clicks-catcher");

        // move the animations el to the upper most layer
        this.animationsEl.appendTo(document.body);

        this.init();
    }

    PresetStoreView.prototype = {
        init: function() {
            this.saveAsEl.click(this.onSaveAsClicked.bind(this));
            this.presetStore.on("presetsLoaded", this.onPresetsLoaded.bind(this));
            this.presetStore.on("presetAdded", this.onPresetAdded.bind(this));
            this.presetStore.on("presetRemoved", this.onPresetRemoved.bind(this));
            this.presetStore.on("activePresetChanged", this.onActivePresetChanged.bind(this));

            this.animationsEl.hide();
            this.outsideLayerEl.click(this.hidePresets.bind(this));
            this.activeAnimationEl.click(this.showPresets.bind(this));
        },

        showPresets: function() {
            var topOffset = this.activeAnimationEl.offset(),
                height = this.activeAnimationEl.outerHeight(),
                width = this.activeAnimationEl.outerWidth();
            this.animationsEl.css("top", topOffset.top + height);
            this.animationsEl.css("left", topOffset.left);
            this.animationsEl.css("width", width);
            this.animationsEl.show();
            this.animationsEl.before(this.outsideLayerEl);
        },

        hidePresets: function() {
            this.animationsEl.hide();
            this.outsideLayerEl.detach();
        },
        
        enableSaveAs: function(){
            this.saveAsEl.removeClass('disabled');
        },

        onSaveAsClicked: function() {
            if (this.presetStore.savePresetAs())
                this.saveAsEl.addClass('disabled');
            return false;
        },

        onDeleteClicked: function(preset) {
            this.presetStore.deletePreset(preset);
            this.hidePresets();
        },

        onPresetsLoaded: function() {
            this.animationsContainerEl.show();
        },

        onPresetAdded: function(preset) {
            var key = "_" + preset;
            if (this.presetsByName.hasOwnProperty(key))
                return;
            var el = $("<li />")
                .append($('<a href="#">')
                    .text(preset)
                    .append($('<span>')
                        .addClass('icon icon-remove')
                        .click(function(){                                                                 
                            var message = "Are you sure you want to delete the '"+ preset +"' preset?";
                            if(window.confirm(message))
                                this.onDeleteClicked(preset)
                         }.bind(this))
                     )
                )
                .click(this.onPresetNameClicked.bind(this, preset))
                .appendTo(this.animationsEl);
            this.presetsByName[key] = el;
            return el;
        },

        onPresetNameClicked: function(preset) {
            this.hidePresets();
            this.presetStore.loadPreset(preset);
            return false;
        },

        onPresetRemoved: function(preset) {
            var key = "_" + preset,
                removedEl = this.presetsByName[key];
            if (!removedEl)
                return;
            delete this.presetsByName[key];
            removedEl.remove();
        },

        onActivePresetChanged: function(newPreset, oldPreset) {
            var oldEl = this.presetsByName["_" + oldPreset],
                newEl = this.presetsByName["_" + newPreset];
            if (oldEl) 
                oldEl.removeClass("current");
            if (!newEl)
                newEl = this.onPresetAdded(newPreset);
            newEl.addClass("current");
            this.activeAnimationEl.text(newPreset);
        }

    };

    Global.PresetStoreView = PresetStoreView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
	function LoadingProgressView(el) {
		this.el = el;
		this.labelEl = el.find(".label");
		this.progressBarEl = el.find(".progress-bar");
		this.progressBarInteriorEl = $("<div />").addClass("progress-bar-interior").appendTo(this.progressBarEl);
	}

	LoadingProgressView.prototype = {
		setValue: function(value) {
			var progress = Math.round(Math.max(0, Math.min(1, value)) * 100) + "%";
			this.progressBarInteriorEl.css("width", progress);
			this.labelEl.text("Loading " + progress);
		},
		hide: function() {
			$(this.el).hide();
		}
	};

	Global.LoadingProgressView = LoadingProgressView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function LogoView(filterDock, filterStore) {
        this.filterDock = filterDock;
        this.filterStore = filterStore;
        this.filterDockToggleEl = $("#filter-store-toggle");
        this.importFilterEl = $("#import-filter");
        this.importFilterEl.click(this.onImportFilterClicked.bind(this));
        this.outsideLayerEl = $("<div />").addClass("clicks-catcher");
        this.dockPanel = new Global.DockPanel("Logo");
        this.logoEl = $("#logo").appendTo(this.dockPanel.el);
        this.init();
    }

    LogoView.prototype = {
        init: function() { 
            this.filterDock.containerEl.hide();
            this.outsideLayerEl.click(this.hideFilterDock.bind(this));
            this.filterDockToggleEl.click(this.showFilterDock.bind(this));
        },

        showFilterDock: function() {
            this.filterDockToggleEl.addClass("selected");

            var offset = this.filterDockToggleEl.offset(),
                width = this.filterDockToggleEl.outerWidth(),
                height = this.filterDockToggleEl.outerHeight();
            this.filterDock.containerEl
                .css("left", offset.left + width / 2)
                .css("top", offset.top + height);

            this.filterDock.containerEl.show();
            this.filterDock.containerEl.before(this.outsideLayerEl);

            return false;
        },

        hideFilterDock: function() {
            this.filterDockToggleEl.removeClass("selected");
            this.outsideLayerEl.detach();
            this.filterDock.containerEl.hide();

            return false;
        },

        onImportFilterClicked: function() {
            var url = prompt("GitHub gist ID or url: ");
            if (!url)
                return;
            this.filterStore.loadFromGitHub(url, function(err, filter) {
                if (err)
                    console.error(err);
            });
        }
    }

    Global.LogoView = LogoView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function HelpView(app) {
        this.app = app; 
        this.helpPopupEl = $("#help-popup").detach();
        this.helpPopupCloseEl = $('#help-popup-close', this.helpPopupEl);
        this.helpNav = $('#help-nav a', this.helpPopupEl);
        this.helpArticle = $('article', this.helpPopupEl);

        this.init();
    }

    HelpView.prototype = {
        init: function() {
            $(document).on('click', '#help-link', this.onHelpClicked.bind(this))
            this.helpPopupCloseEl.click(this.onHelpPopupCloseClicked.bind(this));
            this.helpNav.click(this.navigateTo.bind(this));
            
            // select the first help tab
            this.helpNav.eq(0).click();
        },

        show: function(){
            this.helpPopupEl.appendTo(document.body).removeClass("hidden").show();
        },

        onHelpClicked: function(e) {
            this.show();
            return false;
        },

        onHelpPopupCloseClicked: function() { 
            this.helpPopupEl.detach();
            
            if (this.app.firstRun) {
                this.app.fileSystem.save("first_run", "no");
                this.app.firstRun = false;
            }

            return false;
        },
        
        navigateTo: function(e){   
            var target = e.currentTarget || e[0].currentTarget;
            this.helpArticle.find('div').hide().filter(target.hash).show();
            this.helpNav.removeClass('selected').filter($(target)).addClass('selected');
            return false;
        }
    }

    Global.HelpView = HelpView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function ImportFilterView(filterStore) {
        this.filterStore = filterStore;
        
        this.dockPanel = new Global.DockPanel("Import via Gist");
        this.el = $("#import-filter").appendTo(this.dockPanel.el);
        this.hasErrorState = false;

        this.importErrorPrompt = this.el.find('.error');
        this.importFilterForm = this.el.find('form');
        this.importFilterButton = this.importFilterForm.find('.button');
        this.importFilterInput = this.importFilterForm.find('input[name="gist-url"]');
        
        this.init();
    }

    ImportFilterView.prototype = {
        init: function() { 
            this.importFilterForm.on('submit', this.onImportFilter.bind(this));
            this.importFilterButton.on('click', this.onImportFilter.bind(this));
            this.importFilterInput.on('keyup paste', this.onFilterInputChange.bind(this));
        },

        onImportFilterError: function() {
            this.hasErrorState = true;
            this.showErrorState();
        },
        
        showErrorState: function(){
            this.importErrorPrompt.show();
            this.importFilterInput.addClass('error');
        },
        
        removeErrorState: function(){
            this.importErrorPrompt.hide();
            this.importFilterInput.removeClass('error');
        },
        
        onFilterInputChange: function(e){
            this.importFilterButton.toggleClass('disabled', !this.importFilterInput.is(':valid'))
            this.importFilterInput.removeClass('error');
        },

        onImportFilter: function(e) {
            e.preventDefault() 
            
            if (!this.importFilterInput.is(':valid'))
                return
            
            var url = this.importFilterInput.val(),
                self = this;
                
            this.filterStore.loadFromGitHub(url, function(err, filter) {
                if (err){
                    self.onImportFilterError()
                    return
                }

                if (self.hasErrorState){
                    self.removeErrorState()
                }
                
                self.importFilterInput.val('')
            });
        }
    }

    Global.ImportFilterView = ImportFilterView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    function ShaderEditorView(filterList, github) {
        this.filterList = filterList;
        this.github = github;

        this.configEditorEl = $("#config-editor");
        this.shaderNameEl = this.configEditorEl.find(".config-shader-name");
        this.shaderMixBlendModeEl = this.configEditorEl.find(".config-shader-mix-blend-mode");
        this.shaderMeshColumnsEl = this.configEditorEl.find(".config-shader-mesh-columns");
        this.shaderMeshRowsEl = this.configEditorEl.find(".config-shader-mesh-rows");
        this.configParametersEl = this.configEditorEl.find(".config-parameters");
        this.configParametersWrapperEl = this.configEditorEl.find(".config-parameters-wrapper");
        this.configParameterEl = this.configEditorEl.find(".config-parameter").detach();

        this.vertexShaderEditorEl = $("#vertex-shader .code-view");
        this.fragmentShaderEditorEl = $("#fragment-shader .code-view");

        this.errorsEl = $("#shader-errors");
        this.vertexShaderErrorsEl = $("#shader-errors .vertex-shader");
        this.fragmentShaderErrorsEl = $("#shader-errors .fragment-shader");

        // error for missing CodeMirror lib
        this.codeViewErrorEl = $("#code-view-error");

        this.saveToGistEl = $("#post-gist-button");
        this.saveToGistEl.click(this.onSaveToGistClicked.bind(this));   

        this.githubPopupEl = $("#github-popup");
        this.githubLinkEl = $("#gist-link");
        this.githubIframeEl = $("#github-login");
        this.githubPublicGistEl = $("#save-public-gist");
        this.githubPopupCloseEl = this.githubPopupEl.find(".gist-popup-close");
        this.githubPopupCloseEl.click(this.onCloseGistPopupClicked.bind(this));   
        this.githubSaveGistWithLoginEL = $("#save-gist-with-login");
        this.githubSaveGistWithLoginEL.click(this.onSaveGistWithLoginClicked.bind(this));
        this.githubLoginLinkEl = $("#github-login-link");
        this.githubLoginLinkEl.click(this.onGithubLoginLinkClicked.bind(this));
        this.githubSaveAnonymousGistEl = $("#save-anonymous-gist");
        this.githubSaveAnonymousGistEl.click(this.onSaveAnonymousGistClicked.bind(this));   

        this.loaderView = new Global.LoadingProgressView($("#shader-errors .loader"));
        this.angleLib = new Global.AngleLib();
        this.angleLib.on("progress", this.onAngleLibLoadingProgress.bind(this));
        this.angleLib.on("completed", this.onAngleLibLoadingCompleted.bind(this));

        this.vertexShaderEditor = new Global.ShaderCodeEditorView(this.angleLib, "vertex",
                this.vertexShaderEditorEl, this.vertexShaderErrorsEl);

        this.fragmentShaderEditor = new Global.ShaderCodeEditorView(this.angleLib, "fragment",
                this.fragmentShaderEditorEl, this.fragmentShaderErrorsEl);
        
        // Use the timer to avoid updating the shader after each keystroke.
        this.shaderTextChangeTimers = {
            "vertex": new Global.Timer(300),
            "fragment": new Global.Timer(300)
        };
        this.shaderTextChangeTimers["vertex"].on("timerFired", this.onShaderTextChangedTimerFired.bind(this, "vertex"));
        this.shaderTextChangeTimers["fragment"].on("timerFired", this.onShaderTextChangedTimerFired.bind(this, "fragment"));

        this.shaderConfigChangeTimer = new Global.Timer(300);
        this.shaderConfigChangeTimer.on("timerFired", this._filterModelChanged.bind(this));

        this.vertexShaderEditor.on("valueChanged", this.onShaderTextChanged.bind(this, "vertex"));
        this.vertexShaderEditor.on("uniformsDetected", this.onShaderUniformsDetected.bind(this, "vertex"));

        this.fragmentShaderEditor.on("valueChanged", this.onShaderTextChanged.bind(this, "fragment"));
        this.fragmentShaderEditor.on("uniformsDetected", this.onShaderUniformsDetected.bind(this, "fragment"));


        this.configDockPanel = new Global.DockPanel("Filter Configuration");
        $("#config-editor").appendTo(this.configDockPanel.el);

        this.vertexDockPanel = new Global.DockPanel("Vertex Shader");
        $("#vertex-shader").appendTo(this.vertexDockPanel.el);
        this.vertexDockPanel.on("activeStateChanged", this.onActiveStateChanged.bind(this, this.vertexShaderEditor));

        this.fragmentDockPanel = new Global.DockPanel("Fragment Shader");
        $("#fragment-shader").appendTo(this.fragmentDockPanel.el);
        this.fragmentDockPanel.on("activeStateChanged", this.onActiveStateChanged.bind(this, this.fragmentShaderEditor));
        
        this.errorsDockPanel = new Global.DockPanel("Errors");
        this.errorsEl.appendTo(this.errorsDockPanel.el);
        
        this.onWindowResizedCallback = this.onWindowResized.bind(this);
        this.onDockResizedCallback = this.onDockResized.bind(this);

        this.panelsAdded = false;
        this.filterModel = null;
    }

    ShaderEditorView.prototype = {
        
        onAngleLibLoadingProgress: function(value) {
            this.loaderView.setValue(value);
        },

        onAngleLibLoadingCompleted: function() {
            this.loaderView.hide();
        },

        show: function() {
            if (!this.panelsAdded) {
                this.panelsAdded = true;
                
                this.editorColumn = this.dockView.addVerticalColumn()

                this.editorColumn.addCloseButton()
                        .setMinSize(400)
                        .setWidth(600)
                        .addContainer()
                            .add(this.configDockPanel)
                            .add(this.vertexDockPanel)
                            .add(this.fragmentDockPanel)
                        .column
                            .addContainer()
                                .setHeight(100)
                                .add(this.errorsDockPanel)
                        .column
                            .on("columnClosed", this.onColumnClosed.bind(this))
                                                                               
                // move the github save button in the tablist
                this.editorColumn.items[0].tabListEl.append(this.saveToGistEl);
                
                $("body").bind("dockViewResized", this.onDockResizedCallback);
                window.addEventListener("resize", this.onWindowResizedCallback, false);
            }
        },
        
        hide: function(){
            this.editorColumn.onColumnCloseButtonClicked()
        },
        
        onColumnClosed: function() {
            $("body").unbind("columnResized", this.onDockResizedCallback);
            window.removeEventListener("resize", this.onWindowResizedCallback, false);
            this.panelsAdded = false;
            this.configDockPanel.setActive(false);
            this.vertexDockPanel.setActive(false);
            this.fragmentDockPanel.setActive(false);
        },

        updateModel: function(filterModel) {
            var self = this;

            this.configEditorEl.find(".config-parameter").remove();

            this.filterModel = new Global.ActiveObject(filterModel);

            this.updateTitle();
            
            this.filterModel
                .bindToTextInput("label", this.shaderNameEl);

            var mix = this.filterModel.get("mix");
            if (!mix)
                mix = this.filterModel.set("mix", {"blendMode": "normal"});
            mix.bindToSelectInput("blendMode", this.shaderMixBlendModeEl);
            
            var mesh = this.filterModel.get("mesh");
            if (!mesh)
                mesh = this.filterModel.set("mesh", {"columns": 1, "rows": 1});
            mesh.bindToSelectInput("columns", this.shaderMeshColumnsEl)
                .bindToSelectInput("rows", this.shaderMeshRowsEl);

            var params = this.filterModel.get("params");
            self.configParametersEl.empty();

            var paramsByKey = {};
            var first = true;
            $.each(params.properties, function(key, value) {
                var item = self.createParameterItem(key, first);
                paramsByKey["_" + key] = item;
                first = false;
            });
            params.on("propertyAdded", function(key, value) {
                var item = self.createParameterItem(key);
                paramsByKey["_" + key] = item;
            });
            params.on("propertyRemoved", function(key) {
                var item = paramsByKey["_" + key];
                delete paramsByKey["_" + key];
                if (!item)
                    return;
                if (self._activeParameter == key)
                    self.configEditorEl.find(".config-parameter").remove();
                item.remove();
            });
            
            this.filterModel.on("rootValueChanged", this.filterModelChanged.bind(this));
        },

        createParameterItem: function(key, first) {
            var el = $("<li />")
                .text(key)
                .appendTo(this.configParametersEl);
            el.click(this.onParamterItemClicked.bind(this, el, key));
            if (first)
                this.activateParameter(el, key);
            return el;
        },

        onParamterItemClicked: function(el, key) {
            this.activateParameter(el, key);
            return false;
        },

        activateParameter: function(parameterEl, key) {
            var values = this.filterModel.get("params"),
                config = this.filterModel.get("config"),
                self = this;
            this._activeParameter = key;

            this.configParametersEl.find("li").removeClass("active");
            parameterEl.addClass("active");

            config.bind(key, function(paramConfig) {
                // Param config can be null when the value is deleted
                if (!paramConfig)
                    return;
                var el = self.configParameterEl.clone();
                self.configEditorEl.find(".config-parameter").remove();
                self.configParametersWrapperEl.after(el);
                el.find(".config-parameter-label").text(key);
                el.find(".config-parameter-delete").click(function() {
                    config.remove(key);
                    values.remove(key);
                    return false;
                });
                
                paramConfig.bindToSelectInput("type", el.find(".config-parameter-type"));
                paramConfig.bind("type", function(type, oldValue) {
                    if (!oldValue || type == oldValue)
                        return;
                    var valueType = self.parametersByType[type];
                    if (!valueType)
                        return;
                    values.set(key, valueType.value);
                    config.set(key, valueType.config);
                });

                if (paramConfig.get("type") == "warp" && !values.get(key))
                    values.set(key, Global.WarpHelpers.generateWarpPoints());
                
                var defaultValueEl = el.find(".config-default-value");
                defaultValueEl.empty();

                var EditorClass = Global.Controls.get(paramConfig.get("type"));
                if (!EditorClass)
                    return;

                var editor = new EditorClass(self, key, config.toJSON()),
                    table = $("<table class='paramsTable' />").appendTo(defaultValueEl),
                    tr = $("<tr class='field' />")
                        .appendTo(table);
                editor.pushControls(tr);
                editor.setSource(values);

                var configParameterRangeEl = el.find(".config-parameter-range");
                configParameterRangeEl.hide();
                if (self.rangeTypes.indexOf(paramConfig.get("type")) != -1) {
                    configParameterRangeEl.show();
                    paramConfig.bindToTextInput("min", configParameterRangeEl.find(".config-parameter-range-min"));
                    paramConfig.bindToTextInput("max", configParameterRangeEl.find(".config-parameter-range-max"));
                    paramConfig.bindToTextInput("step", configParameterRangeEl.find(".config-parameter-range-step"));
                    paramConfig.bind("min", editor.updateConfig.bind(editor, paramConfig));
                    paramConfig.bind("max", editor.updateConfig.bind(editor, paramConfig));
                    paramConfig.bind("step", editor.updateConfig.bind(editor, paramConfig));
                }
            });
        },

        loadFilter: function(filter) {
            // Prevent change events while loading first version.
            this.filter = null;

            this.show();

            this.filter = filter;
            this.updateModel(filter.original);

            // tripwire for missing CodeMirror library
            if (this.vertexShaderEditor.isSupported){
                this.vertexShaderEditor.setValue(filter.editedSource_vertex || "");
                this.fragmentShaderEditor.setValue(filter.editedSource_fragment || "");
            } else if (!this.haveAddedCodeMirrorError) {
                this.haveAddedCodeMirrorError = true;
                this.vertexShaderEditorEl.append(this.codeViewErrorEl.clone(true).show())
                this.fragmentShaderEditorEl.append(this.codeViewErrorEl.clone(true).show())
            }
            
            if (this.lastEditor)
                this.lastEditor.refresh();
        },

        onShaderTextChanged: function(type, newValue) {
            if (!this.filter)
                return;
            this.shaderTextChangeTimers[type].invoke([this.filter, newValue]);
        },

        onShaderTextChangedTimerFired: function(type, filter, newValue) {
            this.filterList.filterUpdate(filter, type, newValue);
        },

        defaultUniforms: ["u_meshBox", "u_tileSize", "u_meshSize", "u_projectionMatrix", "u_texture", "u_textureSize", "u_contentTexture"],
        onShaderUniformsDetected: function(type, uniforms) {
            if (!this.filter)
                return;
            var self = this,
                hadChanges = false;
            $.each(uniforms, function(i, uniform) {
                if (self.defaultUniforms.indexOf(uniform.name) != -1)
                    return;
                if (self.addUniform(uniform))
                    hadChanges = true;
            });
            if (hadChanges) 
                this.updateFilterConfig();
        },

        rangeTypes: ["range", "vec2", "vec3", "vec4"],

        parametersByType: {
            "color": {
                config: {
                    type: 'color',
                    mixer: 'mixVector',
                    generator: 'vector'
                },
                value: [0, 0, 0, 1]
            },
            "unknown": {
                config: {
                    type: 'unknown'
                },
                value: "0"
            },
            "checkbox": {
                config: {
                    type: 'checkbox',
                    mixer: 'dontMix'
                },
                value: 0
            },
            "range": {
                config: {
                    type: 'range',
                    min: -1000,
                    max: 1000,
                    step: 0.01
                },
                value: 0
            },

            "vec2": {
                config: {
                    type: 'vec2',
                    generator: 'vector',
                    mixer: 'mixVector',
                    min: -1000,
                    max: 1000,
                    step: 0.01
                },
                value: [0, 0]
            },

            "vec3": {
                config: {
                    type: 'vec3',
                    generator: 'vector',
                    mixer: 'mixVector',
                    min: -1000,
                    max: 1000,
                    step: 0.01
                },
                value: [0, 0, 0]
            },

            "vec4": {
                config: {
                    type: 'vec4',
                    generator: 'vector',
                    mixer: 'mixVector',
                    min: -1000,
                    max: 1000,
                    step: 0.01
                },
                value: [0, 0, 0, 0]
            },

            "transform": {
                config: {
                    type: 'transform',
                    generator: 'transform',
                    mixer: {
                        fn: 'mixHash',
                        params: ['mixNumber']
                    }
                },
                value: {
                    rotationX: 0,
                    rotationY: 0,
                    rotationZ: 0
                }
            },

            "warp": {
                config: {
                    type: 'warp',
                    generator: 'warpArray',
                    mixer: 'mixVectorOfVectors'
                },
                value: null
            }
        },

        isSameType: function(oldType, newType) {
            if (oldType == newType || 
               (oldType == "color" && newType == "vec4") ||
               (oldType == "checkbox" && newType == "range") ||
               (newType == "unknown") ||
               (oldType == "unknown"))
                return true;

            return false;
        },

        addUniform: function(uniform) {
            var value;
            if (uniform.size != 1)
                return false;
            switch (uniform.type) {
            case "vec2":
                value = this.parametersByType["vec2"];
                break;
            case "vec3":
                value = this.parametersByType["vec3"];
                break;
            case "vec4":
                value = this.parametersByType["vec4"];
                break;
            case "float":
                value = this.parametersByType["range"];
                break;
            case "mat4":
                value = this.parametersByType["transform"];
                break;
            default:
                value = this.parametersByType["unknown"];
                break;
            }

            if (!value)
                return false;
            
            if (this.filter.original.config.hasOwnProperty(uniform.name) &&
                this.isSameType(this.filter.original.config[uniform.name].type, value.config.type))
                return false;

            this.filter.original.config[uniform.name] = value.config;
            this.filter.original.params[uniform.name] = value.value;
            return true;
        },

        updateFilterConfig: function() {
            this.updateModel(this.filter.original);
            this.filter.reload(this.filter.original);
            this.filterList.forkConfigUpdate(this.filter);
        },

        filterModelChanged: function() {
            if (!this.filter || !this.filterModel)
                return;
            this.shaderConfigChangeTimer.invoke([]);
        },

        _filterModelChanged: function() {
            if (!this.filter || !this.filterModel)
                return;
            this.updateTitle();
            var jsonFilterModel = this.filterModel.toJSON();
            this.filter.reload(jsonFilterModel);
            this.filterList.forkConfigUpdate(this.filter);
        },

        updateTitle: function() {
            if (!this.filterModel)
                return;
            this.configDockPanel.container.column.setTitle("Editing shader: " + this.filterModel.label);
        },

        onWindowResized: function() {
            if (!this.lastEditor)
                return;
            this.lastEditor.refresh();
        },

        onDockResized: function(ev, direction) {
            if (!this.lastEditor)
                return; 
            this.lastEditor.refresh(); 
        },

        onActiveStateChanged: function(editor, active) {
            if (active) {
                this.lastEditor = editor;
                this.lastEditor.setActive(true);
            } else {
                editor.setActive(false);
            }
        },

        onSaveToGistClicked: function() {
            if (!this.filter)
                return false;
            this.githubPopupEl
                .removeClass("saved saving login")
                .addClass("info")
                .show();
            return false;
        },

        onSaveGistWithLoginClicked: function() {
            this.githubPopupEl
                .removeClass("saved saving info")
                .addClass("login")
                .show();
            this.onGithubLoginCallback = this.github.on("login", this.onGithubLogin.bind(this));
            this.openGithubLogin();
            return false;
        },

        openGithubLogin: function() {
            var loginUrl = this.github.getLoginUrl();
            window.open(loginUrl, "css_filterlab_github_login", "width=1015,height=500");
        },

        onGithubLoginLinkClicked: function() {
            this.openGithubLogin();
            return false;
        },

        onGithubLogin: function(err, token) {
            console.log(err, token);
            if (err) {
                this.githubPopupEl
                    .addClass("login-failed")
                    .show();
            } else {
                this.saveGist(token);
            }
        },

        onSaveAnonymousGistClicked: function() {
            this.saveGist(null);
            return false;
        },

        saveGist: function(token) {
            if (!this.filter)
                return;
            
            this.githubPopupEl
                .removeClass("info login")
                .addClass("saving");

            var filter = this.filter,
                data = filter.getData(),
                self = this,
                filterConfig = filter.original,
                name = filterConfig.label || filter.name || "Filter";
            filterConfig.createdWith = "CSS FilterLab";
            
            var files = {
                    "config.json": {
                        "content": JSON.stringify(filterConfig, null, 4)
                    },
                    "shader.vs": {
                        content: filter.editedSource_vertex
                    },
                    "shader.fs": {
                        content: filter.editedSource_fragment
                    }
                };

            var publicGist = this.githubPublicGistEl.is(":checked");
            
            // anonymous gists cannot be patched
            this.github.postGist(token, null, publicGist, name, files, function(response) {
                data.gist_id = response.id;
                data.gist_html_url = response.html_url;
                self.filterList.forkConfigUpdate(filter);

                self.githubLinkEl.attr("href", response.html_url).text(response.html_url);
                self.githubPopupEl.removeClass("saving").addClass("saved");
            });
        },

        removeGithubCallback: function() {
            if (this.onGithubLoginCallback) {
                this.github.off("login", this.onGithubLoginCallback);
                this.onGithubLoginCallback = null;
            }
        },

        onCloseGistPopupClicked: function() {
            this.removeGithubCallback();
            this.githubPopupEl.hide();
            return false;
        }

    }   

    Global.ShaderEditorView = ShaderEditorView;
})();

/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function ShaderCodeEditorView(angleLib, shaderType, codeEditorEl, errorsPanelEl) {
        ShaderCodeEditorView.$super.call(this, codeEditorEl);
        this.angleLib = angleLib;
        this.shaderType = shaderType;
        this.codeEditorEl = codeEditorEl;
        this.errorsPanelEl = errorsPanelEl;
        this.lastCompileErrorMarkers = [];
        this.on("valueChanged", this.onShaderTextChanged.bind(this));
    }

    Global.Utils.extend(ShaderCodeEditorView).from(Global.CodeEditor);

    $.extend(ShaderCodeEditorView.prototype, {

        onShaderTextChanged: function(newValue) {
            var self = this;
    
            // Remove old errors.            
            this.errorsPanelEl.empty();
            $.each(this.lastCompileErrorMarkers, function(i, error) {
                self.editor.clearMarker(error.gutterMarker);
                error.inlineMarker.clear();
            });
            this.lastCompileErrorMarkers = [];

            this.angleLib.compile(this.shaderType, newValue, function(err, data) {
                if (err) {
                    console.error(err);
                    return;
                }

                // If the code changed since we requested this result, just ignore it.
                if (data.original != self.getValue())
                    return;

                if (!data.errors) {
                    self.fire("uniformsDetected", [data.uniforms]);
                    return;
                }

                $("<div />").addClass("header").text(Global.Utils.upperCaseFirstLetter(self.shaderType) + 
                    " shader errors").appendTo(self.errorsPanelEl);

                $.each(data.errors, function(i, error) {
                    $("<pre />")
                        .append($("<span class='error-type' />").text(error.type))
                        .append($("<span class='error' />").text(error.error))
                        .appendTo(self.errorsPanelEl);
                    
                    var pos = self.editor.posFromIndex(error.index),
                        inlineMarker = self.editor.markText(pos, {line: pos.line, ch: pos.ch + 1}, "error-bookmark"),
                        gutterMarker = self.editor.setMarker(pos.line, "%N%", "error-line");
                    self.lastCompileErrorMarkers.push({
                        inlineMarker: inlineMarker,
                        gutterMarker: gutterMarker
                    });
                });
            });            
        }

    });

    Global.ShaderCodeEditorView = ShaderCodeEditorView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {

    function TimelineView(animation) {
        this.el = $("#timeline")

        this.animation = animation;
        this.animation.on("timeUpdated", this.onTimeUpdated.bind(this));
        this.animation.on("newFrameSelected", this.onNewFrameSelected.bind(this));
        this.animation.on("keyframeAdded", this.onKeyframeAdded.bind(this));
        this.animation.on("keyframeRemoved", this.onKeyframeRemoved.bind(this));
        this.animation.on("durationUpdated", this.onDurationUpdated.bind(this));

        this.doc = $(this.el[0].ownerDocument);
        this.keyframeListEl = this.el.find(".keyframes_list");
        this.playHeadEl = this.el.find(".play_head");

        this.activeKeyFrameEl = $("#toggleActiveKeyframeButton");
        this.prevKeyFrameEl = $("#gotoPrevKeyframeButton");
        this.nextKeyFrameEl = $("#gotoNextKeyframeButton");
        this.playPauseEl = $("#playPauseButton");

        this.timelineDurationEl = $("#timelineDuration");

        this.dockPanel = new Global.DockPanel("Timeline");
        this.el.appendTo(this.dockPanel.el);

        this.isPlaying = false;
        this.startSystemTime = null;
        this.startPlayTime = null;

        this.init();
    }

    TimelineView.prototype = {
        init: function() {
            var self = this;
            this.playHeadEl.mousedown(function(e) {
                self.onHeadMouseDown(e);
            });
            this.activeKeyFrameEl.click(function() {
                self.animation.toggleKeyframe();
            });
            this.playPauseEl.click(function() {
                self.playPause();
            });
            this.prevKeyFrameEl.click(function() {
                self.animation.setActiveKeyframe(self.animation.prevKeyframe());
            });
            this.nextKeyFrameEl.click(function() {
                self.animation.setActiveKeyframe(self.animation.nextKeyframe());
            });
            this.timelineDurationEl.bind("click keyup", function(e) {
                self.animation.setDuration(self.getDuration());
            });
            this.keyframeListEl.bind("click", self.onTimelineClick.bind(self));

            this.updatePlayStateCallback = function() {
                self.updatePlayState();
            }
        },

        getDuration: function() {
            return parseFloat(this.timelineDurationEl.val() * 1000);
        },

        setDuration: function(duration) {
            if (this.getDuration() == duration)
                return;
            this.timelineDurationEl.val(duration / 1000);
        },

        updateTimelineControls: function() {
            this.playHeadEl.css("left", this.animation.currentTime() + "%");
            this.playPauseEl.toggleClass("active", this.isPlaying);
            var keyframe = this.animation.currentKeyframe();
            this.activeKeyFrameEl.toggleClass("active", keyframe && !keyframe.generated);
            this.prevKeyFrameEl.toggleClass("active", !!this.animation.prevKeyframe());
            this.nextKeyFrameEl.toggleClass("active", !!this.animation.nextKeyframe());
        },

        onDurationUpdated: function(duration) {
            this.setDuration(duration);
            this.isPlaying = false;
            this.updateTimelineControls();
        },

        maxTime: function() {
            return this.keyframeListEl.outerWidth();
        },
        
        onTimelineClick: function(e){
            var timelineOffset = $(this.keyframeListEl).offset(),
                timelineWidth = $(this.keyframeListEl).width(),
                delta = e.pageX - timelineOffset.left,
                newTime = Math.min((delta * 100) / timelineWidth, 100);
                
            this.animation.setCurrentTime(newTime);
        },

        onHeadMouseDown: function(e) {
            e.preventDefault();
            var self = this,
                playHeadLastMousePosition = event.pageX,
                playHeadLastPosition = this.animation.currentTime(),
                maxTime = this.maxTime();

            function mousemove(e) {
                e.preventDefault();
                var delta = (event.pageX - playHeadLastMousePosition) / maxTime * 100,
                    newTime = Math.min(Math.max(playHeadLastPosition + delta, 0), 100);
                self.animation.setCurrentTime(newTime);
            }

            function mouseup(e) {
                e.preventDefault();
                self.doc.unbind("mousemove", mousemove);
                self.doc.unbind("mouseup", mouseup);
            }

            this.doc.mousemove(mousemove);
            this.doc.mouseup(mouseup);
        },

        onKeyframeAdded: function(keyframe) {
            var self = this,
                el = $("<div class='keyframe'></div>")
                .appendTo(this.keyframeListEl)
                .css("left", keyframe.time + "%");

            keyframe.el = el;
            
            // don't let the timeline catch this because it will set a new time.
            el.on('click', function(e){
                e.stopPropagation()
            })
            
            el.mousedown(function(e) {
                e.preventDefault();
                var frameLastMousePosition = event.pageX,
                    frameLastPosition = keyframe.time,
                    maxTime = self.maxTime();
                
                self.animation.setCurrentTime(keyframe.time);

                function mousemove(e) {
                    e.preventDefault();
                    var delta = (event.pageX - frameLastMousePosition) / maxTime * 100,
                        newTime = Math.min(Math.max(frameLastPosition + delta, 0), 100);
                    keyframe.time = newTime;
                    el.css("left", keyframe.time + "%");
                    self.animation.sortKeyframes();
                    self.animation.setCurrentTime(keyframe.time);
                    self.animation.saveAnimation();
                }

                function mouseup(e) {
                    e.preventDefault();
                    self.doc.unbind("mousemove", mousemove);
                    self.doc.unbind("mouseup", mouseup);
                }

                self.doc.mousemove(mousemove);
                self.doc.mouseup(mouseup);
            });
        },
        
        updatePlayState: function() {
            if (!this.isPlaying)
                return;
            var systemTime = Date.now(),
                delta = systemTime - this.startSystemTime;

            var newTime = this.startPlayTime + delta / this.animation.duration * 100;
            if (newTime >= 100)
                this.isPlaying = false;
            
            newTime = Math.min(newTime, 100);
            this.animation.setCurrentTime(newTime);
            
            if (this.isPlaying)
                window.webkitRequestAnimationFrame(this.updatePlayStateCallback);
        },
        
        playPause: function() {
            if (this.isPlaying) {
                this.isPlaying = false;
                this.updateTimelineControls();
            } else {
                this.isPlaying = true;
                this.startSystemTime = Date.now();
                this.startPlayTime = this.animation.currentTime();
                if (this.startPlayTime >= 100)
                    this.startPlayTime = 0;
                this.updateTimelineControls();
                window.webkitRequestAnimationFrame(this.updatePlayStateCallback);
            }
        },

        onKeyframeRemoved: function(keyframe) {
            if (!keyframe.el)
                return;
            keyframe.el.remove();
            delete keyframe.el;
        },

        onTimeUpdated: function() {
            this.updateTimelineControls();
        },

        onNewFrameSelected: function(newKeyframe, oldKeyframe) {
            if (oldKeyframe && oldKeyframe.el)
                oldKeyframe.el.removeClass("active");
            if (newKeyframe && newKeyframe.el)
                newKeyframe.el.addClass("active");
        }
        
    }

    Global.TimelineView = TimelineView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function DockColumn(parent, direction) {
        DockColumn.$super.call(this);

        this.parent = parent;
        this.direction = direction;
        this.items = [];

        this.init();
    }

    Global.Utils.extend(DockColumn).from(Global.EventDispatcher);

    $.extend(DockColumn.prototype, {
        minColumnSize: 100,

        init: function() {
            this.el = (this.containerEl || $("<div />"))
                .addClass("panel-column")
                .addClass(this.direction)
                .data("item", this);
            this.columnItemsEl = $("<div />")
                .addClass("panel-column-items")
                .appendTo(this.el);
        },

        setWidth: function(width) {
            this.el.css("-webkit-box-flex", "0")
                   .css("-webkit-flex", 1)
                   .css("width", width);
            return this;
        },

        setHeight: function(height) {
            this.el.css("-webkit-box-flex", "0")
                   .css("-webkit-flex", 1)
                   .css("height", height);
            return this;
        },

        setMinSize: function(minSize) {
            this.minSize = minSize;
            return this;
        },

        _columnHandleEl: function() {
            if (!this.columnHandleEl) {
                this.columnHandleEl = $("<div />")
                    .addClass("panel-column-header")
                    .prependTo(this.el);
            }
            return this.columnHandleEl;
        },

        setTitle: function(title) {
            if (!this.columnTitleEl) {
                this.columnTitleEl = $("<div />")
                    .addClass("panel-column-title")
                    .appendTo(this._columnHandleEl());
            }
            this.columnTitleEl.text(title);
        },

        addCloseButton: function() {
            if (!this.columnCloseButtonEl) {
                this.columnCloseButtonEl = $("<a href='#' />")
                    .addClass("panel-column-close-button")
                    .addClass("icon icon-close")
                    .text("Close")
                    .prependTo(this._columnHandleEl())
                    .click(this.onColumnCloseButtonClicked.bind(this));
            }
            return this;
        },

        onColumnCloseButtonClicked: function() {
            // Remove the items from the parent, so that we don't loose the events.
            this.columnItemsEl.detach();
            // Remove the separator first.
            this.el.prev().remove();
            this.el.remove();
            this.fire("columnClosed");
            $("body").trigger("dockViewResized", [this.parent.direction || "horizontal"]);
            return false;
        },

        add: function(panel) {
            var container;
            if (!this.items.length)
                container = this.addContainer();
            else
                container = this.items[this.items.length - 1];
            container.add(panel);
            return container;
        },

        hasFollowingResizeableContainer: function(nextItem) {
            var i = this.items.indexOf(nextItem);
            if (i == -1)
                return;
            ++i;
            for (; i < this.items.length; ++i) {
                if (!this.items[i].isFixed)
                    return true;
            }
            return false;
        },

        minItemSize: function(item) {
            if (item.minSize !== undefined)
                return item.minSize;
            return this.minColumnSize;
        },

        addSeparator: function() {
            var prev, next, nextItem, prevItem, startDimension, startOffset,
                dimension = (this.direction == "horizontal" ? "width" : "height"),
                dimensionReader = (this.direction == "horizontal" ? "outerWidth" : "outerHeight"),
                offsetName = (this.direction == "horizontal" ? "left" : "top"),
                min, max,
                self = this;
            var separator = $("<div />")
                .addClass("panel-separator")
                .draggable({ 
                    helper: $("<div />"),
                    scroll: false,
                    start: function(event, ui) {
                        next = separator.next();
                        if (next.length === 0)
                            return false;
                        prev = separator.prev();
                        if (prev.length === 0)
                            return false;
                        nextItem = next.data("item");
                        prevItem = prev.data("item");
                        startOffset = ui.helper.offset()[offsetName];
                        startDimension = prev[dimensionReader].call(prev);
                        var separtorDimension = separator[dimensionReader].call(separator);
                        var nextStartDimension = next[dimensionReader].call(next);
                        max = (nextStartDimension - self.minItemSize(nextItem) + startDimension - separtorDimension);
                        min = self.minItemSize(prevItem);
                        if (!self.hasFollowingResizeableContainer(nextItem) && 
                                ((Global.DockView.UsingNewFlexBox && next.css("-webkit-flex") == "0 1 1px") || 
                                (!Global.DockView.UsingNewFlexBox && next.css("-webkit-box-flex") == "0")))
                        {
                            prev.css("-webkit-box-flex", "0")
                                .css("-webkit-flex", 1)
                                .css(dimension, startDimension);
                            next.css("-webkit-box-flex", "1")
                                .css("-webkit-flex", "")
                                .css(dimension, "");
                        } else {
                            prev.css("-webkit-box-flex", "0")
                                .css("-webkit-flex", 1);
                        }
                        if (!Global.DockView.UsingNewFlexBox) {
                            // Workaround bug in WebKit where the new flex is only taken into account
                            // when the flex box is recreated.
                            self.el.css("display", "block");
                            // Force a layout.
                            self.el.css("display");
                            self.el.css("display", "");
                        }
                        $(document.body).addClass("resizing-dock wait-dock-layout");
                    },
                    drag: function(event, ui) {
                        var newDimension = startDimension + (ui.offset[offsetName] - startOffset);
                        prev.css(dimension, Math.min(max, Math.max(min, newDimension)));
                    },
                    stop: function() {
                        $(document.body).removeClass("resizing-dock");
                        $("body").trigger("dockViewResized", [self.direction]);
                        self.fire("columnResized", [dimension]);
                        $(document.body).removeClass("wait-dock-layout");
                        if (prevItem.isDocumentArea) {
                            // Make sure that the document area is always flexible.
                            var savedDimension = next[dimensionReader].call(next);
                            next.css("-webkit-box-flex", "0")
                                .css("-webkit-flex", 1)
                                .css(dimension, savedDimension);
                            prev.css("-webkit-box-flex", "")
                                .css("-webkit-flex", "")
                                .css(dimension, "");
                        }
                    }
                });
            this.columnItemsEl.append(separator);
        },

        addSeparatorIfNeeded: function() {
            if (this.items.length && !this.items[this.items.length - 1].isFixed)
                this.addSeparator();
        },

        addContainer: function(fixed) {
            if (fixed !== true)
                this.addSeparatorIfNeeded();
            var container = new Global.DockContainer();
            this.items.push(container);
            this.columnItemsEl.append(container.el);
            container.column = this;
            return container;
        },

        addVerticalColumn: function() {
            return this._addColumn("vertical");
        },

        addHorizontalColumn: function() {
            return this._addColumn("horizontal");
        },

        _addColumn: function(orientation) {
            this.addSeparatorIfNeeded();
            var column = new Global.DockColumn(this, orientation);
            this.items.push(column);
            this.columnItemsEl.append(column.el);
            column.column = this;
            column.on("columnClosed", this.onColumnClosed.bind(this, column));
            return column;
        },

        onColumnClosed: function(column) {
            var index = this.items.indexOf(column);
            if (index == -1)
                return;
            this.items.splice(index, 1);
        },

        setIsDocumentArea: function() {
            this.isDocumentArea = true;
            return this;
        },
        
        addClass: function(className) {
            this.el.addClass(className)
            return this;
        },

        setFixed: function() {
            this.isFixed = true;
            return this;
        },

        addElements: function(el) {
            this.el.prepend(el);
            return this;
        }

    });

    Global.DockColumn = DockColumn;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
	
	function DockView(containerEl) {
		this.containerEl = containerEl.addClass("dock-root");
		DockView.$super.call(this, null, "horizontal");
		Global.DockView.UsingNewFlexBox = (containerEl.css("display") == "-webkit-flex");
	}

	Global.Utils.extend(DockView).from(Global.DockColumn);
	
	$.extend(DockView.prototype, {
		add: function(panel) {
			var column = this.addVerticalColumn();
			return column.add(panel);
		}
	});

	Global.DockView = DockView;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
    
    function DockContainer() {
        DockContainer.$super.call(this);
        this.items = [];
        this.init();
    }

    Global.Utils.extend(DockContainer).from(Global.EventDispatcher);

    $.extend(DockContainer.prototype, {
        init: function() {
            this.el = $("<div />")
                .addClass("panel-container")
                .data("item", this);

            this.tabListEl = $("<div />")
                .addClass("panel-tab-list")
                .appendTo(this.el);
        },

        add: function(panel) {
            this.items.push(panel);
            this.tabListEl.append(panel.tabEl);
            this.el.append(panel.el);
            panel.setContainer(this);
            if (this.items.length == 1)
                panel.setActive(true);
            return this;
        },

        setHeight: function(height) {
            this.el.css("-webkit-box-flex", "0")
                   .css("-webkit-flex", 1)
                   .css("height", height);
            return this;
        },
        
        setActiveTab: function(tab) {
            $.each(this.items, function(i, item) {
                if (item == tab)
                    return;
                item.setActive(false);
            });
        },

        setIsDocumentArea: function() {
            this.isDocumentArea = true;
            return this;
        },
        
        addClass: function(className) {
            this.el.addClass(className)
            return this;
        },
        
        removeTabs: function() {
            this.tabListEl.remove()
            return this;
        },

        setFixed: function() {
            this.isFixed = true;
            return this;
        }
    });

    Global.DockContainer = DockContainer;

})();
/*
 * Copyright (c) 2012 Adobe Systems Incorporated. All rights reserved.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function() {
	
	function DockPanel(name) {
		DockPanel.$super.call(this);
		this.name = name;
		this.initPanel();
		this.active = false;
	}

	Global.Utils.extend(DockPanel).from(Global.EventDispatcher);
	
	$.extend(DockPanel.prototype, {
		initPanel: function() {
			var self = this;
			this.el = $("<div />")
				.addClass("panel-view");
			this.tabEl = $("<div />")
				.addClass("panel-tab")
				.text(this.name)
				.click(function() {
					self.setActive(true);
					return false;
				});
		},

		setActive: function(active) {
			this.active = active;
			this.el.toggleClass("active", active);
			this.tabEl.toggleClass("active", active);
			if (active && this.container)
				this.container.setActiveTab(this);
			this.fire("activeStateChanged", [active]);
		},

		setContainer: function(container) {
			this.container = container;
		}
	});

	Global.DockPanel = DockPanel;

})();